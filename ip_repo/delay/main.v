`timescale 1ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/29/2016 02:47:46 PM
// Design Name: 
// Module Name: main
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module main(
        input data_in,
        output fifo_en,
        output bram_en,
        input clk,
        input rst_p
    );
    
    parameter DELAY = 2368;
    
    reg [11:0] counter = 12'b0;
    
    localparam WAIT = 2'b00,
               COUNT = 2'b01,
               HIGH = 2'b10;

    wire [1:0] state;
    wire [1:0] next_state;
    reg [1:0] state_reg = WAIT;
    reg [1:0] next_state_reg = WAIT;
    
    assign state = state_reg;
    assign next_state = next_state_reg;
    
    always @(posedge clk) begin
        if(!rst_p) state_reg <= next_state;
        else state_reg <= WAIT;
    end
    
    always @(*) begin
        case(state)
            WAIT: begin
                next_state_reg = (data_in) ? COUNT : WAIT; 
            end
            COUNT: begin
                next_state_reg = (counter == (DELAY - 1)) ? HIGH : COUNT;
            end
            HIGH: begin
                next_state_reg = HIGH;
            end          
            default: begin
                next_state_reg = WAIT;
            end
        endcase
    end
    
    always @(posedge clk) begin
        if(state == COUNT) begin
            counter <= counter + 1'b1;
        end else begin
            counter <= 12'b0;
        end
    end
    
    assign fifo_en = (state == HIGH);
    assign bram_en = ((state == COUNT) && (counter == (DELAY-1))) || (state == HIGH);
endmodule
