`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/08/2016 10:29:53 AM
// Design Name: 
// Module Name: reset_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reset_fsm(
        input clk,
        output clk_rst,
        output rec_reset,
        input locked,
        input rst
    );
    
    localparam INIT = 0,
               CLK_RESET = 1,
               GLB_RESET = 2,
               WAIT = 4,
               DONE = 3;
               
    wire [2:0] state;
    reg [2:0] state_reg = INIT;
    
    assign state = state_reg;
    
    reg [2:0] next_state = INIT;
    
    always @(negedge clk) begin
        state_reg <= next_state;
    end
    
    always @(*) begin
        case(state)
            INIT:       next_state = (rst == 1) ? CLK_RESET : INIT;
            CLK_RESET:   next_state = WAIT;  
            WAIT:       next_state = (locked == 1) ? GLB_RESET : INIT;
            GLB_RESET:  next_state = DONE;
            DONE:       next_state = INIT;
            default:    next_state = INIT;
        endcase
    end
    
    assign clk_rst = (next_state == CLK_RESET);
    assign rec_reset = (next_state == GLB_RESET);
    
endmodule
