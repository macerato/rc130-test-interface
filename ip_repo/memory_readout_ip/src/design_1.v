//Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2015.3 (win64) Build 1368829 Mon Sep 28 20:06:43 MDT 2015
//Date        : Tue Jul 19 14:59:08 2016
//Host        : HEP-PC running 64-bit Service Pack 1  (build 7601)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=21,numReposBlks=21,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,synth_mode=Global}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (Res,
    Res_1,
    bitslip,
    bram_en,
    clk5,
    clk6,
    clk7,
    din_n,
    din_p,
    dout,
    empty,
    full,
    rd_register,
    reset6,
    reset7,
    state_out,
    stop,
    trigger,
    trigger_button);
  output [0:0]Res;
  output [0:0]Res_1;
  output bitslip;
  output bram_en;
  input clk5;
  input clk6;
  input clk7;
  input din_n;
  input din_p;
  output [63:0]dout;
  output empty;
  output full;
  input rd_register;
  input reset6;
  input reset7;
  output state_out;
  output stop;
  input trigger;
  input trigger_button;

  wire bitslip_gen_0_bitslip;
  wire [7:0]blk_mem_gen_0_douta;
  wire [9:0]c_counter_binary_0_Q;
  wire clk5_1;
  wire clk6_1;
  wire clk7_1;
  wire delay_fsm_0_state_out;
  wire delay_fsm_0_stop;
  wire din_n_1;
  wire din_p_1;
  wire [37:0]error_counter_0_Q;
  wire [63:0]fifo_generator_0_dout;
  wire fifo_generator_0_empty;
  wire fifo_generator_0_full;
  wire fifo_reader_0_rd_en;
  wire main_0_bram_en;
  wire rd_register_1;
  wire reset6_1;
  wire reset7_1;
  wire [7:0]reverse_0_dout;
  wire [7:0]selectio_wiz_0_data_in_to_device;
  wire trigger_1;
  wire trigger_button_1;
  wire util_reduced_logic_0_Res;
  wire [7:0]util_vector_logic_0_Res;
  wire [7:0]util_vector_logic_1_Res;
  wire [0:0]util_vector_logic_2_Res;
  wire [0:0]util_vector_logic_3_Res;
  wire [0:0]util_vector_logic_4_Res;
  wire [63:0]xlconcat_0_dout;
  wire [7:0]xlconstant_0_dout;
  wire [7:0]xlconstant_1_dout;
  wire [7:0]xlconstant_2_dout;
  wire [7:0]xlconstant_3_dout;

  assign Res[0] = util_vector_logic_2_Res;
  assign Res_1[0] = util_vector_logic_4_Res;
  assign bitslip = bitslip_gen_0_bitslip;
  assign bram_en = main_0_bram_en;
  assign clk5_1 = clk5;
  assign clk6_1 = clk6;
  assign clk7_1 = clk7;
  assign din_n_1 = din_n;
  assign din_p_1 = din_p;
  assign dout[63:0] = fifo_generator_0_dout;
  assign empty = fifo_generator_0_empty;
  assign full = fifo_generator_0_full;
  assign rd_register_1 = rd_register;
  assign reset6_1 = reset6;
  assign reset7_1 = reset7;
  assign state_out = delay_fsm_0_state_out;
  assign stop = delay_fsm_0_stop;
  assign trigger_1 = trigger;
  assign trigger_button_1 = trigger_button;
  design_1_bitslip_gen_0_0 bitslip_gen_0
       (.bitslip(bitslip_gen_0_bitslip),
        .clk(clk6_1),
        .set(main_0_bram_en),
        .stop(delay_fsm_0_stop));
  design_1_blk_mem_gen_0_0 blk_mem_gen_0
       (.addra(c_counter_binary_0_Q),
        .clka(clk6_1),
        .dina({1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0,1'b0}),
        .douta(blk_mem_gen_0_douta),
        .ena(delay_fsm_0_stop),
        .wea(1'b0));
  design_1_c_counter_binary_0_0 c_counter_binary_0
       (.CE(delay_fsm_0_stop),
        .CLK(clk6_1),
        .Q(c_counter_binary_0_Q),
        .SCLR(reset6_1));
  design_1_delay_fsm_0_0 delay_fsm_0
       (.clk(clk6_1),
        .din(reverse_0_dout),
        .pattern1(xlconstant_0_dout),
        .pattern2(xlconstant_2_dout),
        .pattern3(xlconstant_1_dout),
        .pattern4(xlconstant_3_dout),
        .reset(reset6_1),
        .state_out(delay_fsm_0_state_out),
        .stop(delay_fsm_0_stop));
  design_1_error_counter_0_0 error_counter_0
       (.CLK(clk6_1),
        .Q(error_counter_0_Q),
        .reset(reset6_1),
        .set(delay_fsm_0_stop));
  design_1_fifo_generator_0_0 fifo_generator_0
       (.clk(clk7_1),
        .din(xlconcat_0_dout),
        .dout(fifo_generator_0_dout),
        .empty(fifo_generator_0_empty),
        .full(fifo_generator_0_full),
        .rd_en(fifo_reader_0_rd_en),
        .srst(reset7_1),
        .wr_en(util_vector_logic_3_Res));
  design_1_fifo_reader_0_0 fifo_reader_0
       (.clk(clk6_1),
        .rd_en(fifo_reader_0_rd_en),
        .rd_register(rd_register_1),
        .rst(reset6_1));
  design_1_main_0_0 main_0
       (.bram_en(main_0_bram_en),
        .clk(clk6_1),
        .data_in(util_vector_logic_4_Res),
        .rst_p(reset6_1));
  design_1_reverse_0_0 reverse_0
       (.din(selectio_wiz_0_data_in_to_device),
        .dout(reverse_0_dout));
  design_1_selectio_wiz_0_0 selectio_wiz_0
       (.bitslip(bitslip_gen_0_bitslip),
        .clk_div_in(clk6_1),
        .clk_in(clk5_1),
        .data_in_from_pins_n(din_n_1),
        .data_in_from_pins_p(din_p_1),
        .data_in_to_device(selectio_wiz_0_data_in_to_device),
        .io_reset(clk6_1));
  design_1_util_reduced_logic_0_0 util_reduced_logic_0
       (.Op1(util_vector_logic_1_Res),
        .Res(util_reduced_logic_0_Res));
  design_1_util_vector_logic_0_0 util_vector_logic_0
       (.Op1(reverse_0_dout),
        .Res(util_vector_logic_0_Res));
  design_1_util_vector_logic_1_0 util_vector_logic_1
       (.Op1(util_vector_logic_0_Res),
        .Op2(blk_mem_gen_0_douta),
        .Res(util_vector_logic_1_Res));
  design_1_util_vector_logic_2_0 util_vector_logic_2
       (.Op1(util_reduced_logic_0_Res),
        .Res(util_vector_logic_2_Res));
  design_1_util_vector_logic_3_0 util_vector_logic_3
       (.Op1(delay_fsm_0_stop),
        .Op2(util_vector_logic_2_Res),
        .Res(util_vector_logic_3_Res));
  design_1_util_vector_logic_4_0 util_vector_logic_4
       (.Op1(trigger_1),
        .Op2(trigger_button_1),
        .Res(util_vector_logic_4_Res));
  design_1_xlconcat_0_0 xlconcat_0
       (.In0(blk_mem_gen_0_douta),
        .In1(util_vector_logic_0_Res),
        .In2(c_counter_binary_0_Q),
        .In3(error_counter_0_Q),
        .dout(xlconcat_0_dout));
  design_1_xlconstant_0_0 xlconstant_0
       (.dout(xlconstant_0_dout));
  design_1_xlconstant_0_1 xlconstant_1
       (.dout(xlconstant_1_dout));
  design_1_xlconstant_0_2 xlconstant_2
       (.dout(xlconstant_2_dout));
  design_1_xlconstant_0_3 xlconstant_3
       (.dout(xlconstant_3_dout));
endmodule
