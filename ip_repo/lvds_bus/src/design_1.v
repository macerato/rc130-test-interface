//Copyright 1986-2015 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2015.3 (win64) Build 1368829 Mon Sep 28 20:06:43 MDT 2015
//Date        : Tue Jul 19 15:13:42 2016
//Host        : HEP-PC running 64-bit Service Pack 1  (build 7601)
//Command     : generate_target design_1.bd
//Design      : design_1
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CORE_GENERATION_INFO = "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VERILOG,numBlks=2,numReposBlks=2,numNonXlnxBlks=0,numHierBlks=0,maxHierDepth=0,synth_mode=Global}" *) (* HW_HANDOFF = "design_1.hwdef" *) 
module design_1
   (fast_clk,
    fast_clk_n,
    fast_clk_p,
    slow_clk,
    slow_clk_n,
    slow_clk_p);
  input fast_clk;
  output fast_clk_n;
  output fast_clk_p;
  input slow_clk;
  output slow_clk_n;
  output slow_clk_p;

  wire fast_clk_1;
  wire lvds_buff_0_O;
  wire lvds_buff_0_OB;
  wire lvds_buff_1_O;
  wire lvds_buff_1_OB;
  wire slow_clk_1;

  assign fast_clk_1 = fast_clk;
  assign fast_clk_n = lvds_buff_0_OB;
  assign fast_clk_p = lvds_buff_0_O;
  assign slow_clk_1 = slow_clk;
  assign slow_clk_n = lvds_buff_1_OB;
  assign slow_clk_p = lvds_buff_1_O;
  design_1_lvds_buff_0_0 lvds_buff_0
       (.I(fast_clk_1),
        .O(lvds_buff_0_O),
        .OB(lvds_buff_0_OB));
  design_1_lvds_buff_1_0 lvds_buff_1
       (.I(slow_clk_1),
        .O(lvds_buff_1_O),
        .OB(lvds_buff_1_OB));
endmodule
