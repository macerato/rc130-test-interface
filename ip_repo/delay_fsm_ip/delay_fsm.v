`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2016 01:41:19 PM
// Design Name: 
// Module Name: delay_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module delay_fsm(
        input clk,
        input reset,
        input [7:0] din,
        output stop,
        input [7:0] pattern1,
        input [7:0] pattern2,
        input [7:0] pattern3,
        input [7:0] pattern4,
        output state_out
    );
    
    localparam A = 0,
               B = 1,
               C = 2,
               D = 3,
               DONE = 4;
              
               
    wire [2:0] state;
    reg [2:0] state_reg = A;
    reg [2:0] next_state = A;
    
    assign state_out = state_reg;
    
    assign state = state_reg;
    
    always @(negedge clk) begin
        if(!reset) state_reg <= next_state;
        else       state_reg <= A;
    end
    
    always @(*) begin
        case(state) 
            A:      next_state = (din == pattern1) ? B : A;
            B:      next_state = (din == pattern2) ? C : A;
            C:      next_state = (din == pattern3) ? D : A;
            D:      next_state = (din == pattern4) ? DONE : A;
            DONE:   next_state = DONE;
            default: next_state = A; 
        endcase
    end
    
    assign stop = (state == DONE) ? 1 : 0;
endmodule
