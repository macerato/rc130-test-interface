
`timescale 1 ns / 1 ps

	module mmcm_drp_fsm_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
        input wire CLK,
        output wire CLKout,
        output wire CLKoutb,
        output wire LOCKED_out,
        output wire WAIT_LED,
        
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
	
	wire START;
	
// Instantiation of Axi Bus Interface S00_AXI
	mmcm_drp_fsm_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) mmcm_drp_fsm_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		.frequency_clk(VAL_TO_WRITE),
		.START(START)
	);
	

	// Add user logic here
	
	wire CLKOUT0, DRDY, LOCKED, RST, DEN, DWE;
	wire [15:0] DI;
	wire [15:0] DO;
	wire [6:0] DADDR;
	
	reg [15:0] DI_reg = 16'b0;
	reg [6:0] DADDR_reg = 7'b0;
	reg DEN_reg = 1'b0;
	reg DWE_reg = 1'b0;
	reg RST_reg = 1'b0;
	
	localparam ONE_SHOT_INIT = 3'b001;
	localparam HOT = 3'b010;
	localparam LOW = 3'b011;
	localparam WAIT = 3'b100;
	
	wire one_shot;
	reg one_shot_reg = 1'b0;
	
	assign one_shot = one_shot_reg;
	
    reg [2:0] one_shot_state_reg = ONE_SHOT_INIT;
    reg [2:0] one_shot_next_state = ONE_SHOT_INIT;
    
    wire [2:0] one_shot_state;
    assign one_shot_state = one_shot_state_reg;
	
	always @(posedge CLKg) begin
	   one_shot_state_reg <= one_shot_next_state;
	end
	
	always @* begin
	   case(one_shot_state) 
	       ONE_SHOT_INIT: begin
               one_shot_reg = 1'b0;
               one_shot_next_state = (START) ? HOT : ONE_SHOT_INIT;           
	       end
	       HOT: begin
	           one_shot_reg = 1'b1;
	           one_shot_next_state = LOW;
	       end
	       LOW: begin
	           one_shot_reg = 1'b0;
	           one_shot_next_state = WAIT;
	       end
	       WAIT: begin
	           one_shot_reg = 1'b0;
	           one_shot_next_state = (START) ? WAIT : ONE_SHOT_INIT;
	       end
	       default: begin
	           one_shot_reg = 1'b0;
	           one_shot_next_state = ONE_SHOT_INIT;
	       end
	   endcase
	end
	
	assign DI = DI_reg;
	assign DADDR = DADDR_reg;
	assign DEN = DEN_reg;
	assign DWE = DWE_reg;
	assign RST = RST_reg;
	
	wire [15:0] VAL_TO_WRITE;
	localparam MASK = 16'b0001000000000000;
	localparam CLK1REG_0_ADDR = 7'b0001000;
	
//// FSM to reconfigure DRP /////////////////////
    // all states
	localparam RST_STATE = 4'b0001;
	localparam INIT = 4'b0010;
	localparam ASSERT_RST = 4'b0011;
	localparam SET_DADDR = 4'b0100;
	localparam SET_DEN = 4'b0101;
	localparam WAIT_FOR_DRDY = 4'b0110;
	localparam SET_DI = 4'b0111;
	localparam SET_DEN_DWE = 4'b1000;
	localparam RST_DEN_WAIT_FOR_DRDY = 4'b1001;
    localparam DEASSERT_RST = 4'b1010;
    localparam WAIT_FOR_LOCK = 4'b1011;
    
    wire [3:0] state;
    assign state = state_reg;
    
    reg [3:0] state_reg = INIT;
    reg [3:0] next_state = INIT;
	
	// Assign the next state every clock edge
	always @(posedge CLKg) begin
	   state_reg <= next_state;
	end
	
	reg WAIT_LED_reg = 1'b0;
	assign WAIT_LED = WAIT_LED_reg;
	
	// Next state logic
	always @* begin
	   case(state)
	       RST_STATE: begin
	           DI_reg = 16'b0;
	           DEN_reg = 1'b0;
	           DWE_reg = 1'b0;
	           RST_reg = 1'b0;
	           DADDR_reg = 7'b0;
	           WAIT_LED_reg = 1'b0;
	           
	           next_state = INIT;
	       end
	       INIT: begin
	           DI_reg = 16'b0;
	           DEN_reg = 1'b0;
	           DWE_reg = 1'b0;
	           RST_reg = 1'b0;
	           DADDR_reg = 7'b0;
	           WAIT_LED_reg = 1'b0;
	           
	           next_state = (one_shot) ? ASSERT_RST : INIT;
	           end
	       ASSERT_RST: begin
	           DI_reg = 16'b0;
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               DADDR_reg = 7'b0;
               WAIT_LED_reg = 1'b0;
	       
	           RST_reg = 1'b1;
	           next_state = SET_DADDR;
	           end
	       SET_DADDR: begin
	           DI_reg = 16'b0;
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               RST_reg = 1'b1;
               WAIT_LED_reg = 1'b0;
                          
	           DADDR_reg = CLK1REG_0_ADDR;
	           next_state = SET_DEN;
	           end
	       SET_DEN: begin
	           DI_reg = 16'b0;
               DWE_reg = 1'b0;
               RST_reg = 1'b1;
               DADDR_reg = CLK1REG_0_ADDR;
               WAIT_LED_reg = 1'b0;
                          
	           DEN_reg = 1'b1;
	           next_state = WAIT_FOR_DRDY;
	           end
           WAIT_FOR_DRDY: begin
               DI_reg = 16'b0;
               DWE_reg = 1'b0;
               RST_reg = 1'b1;
               DADDR_reg = CLK1REG_0_ADDR;
               WAIT_LED_reg = 1'b0;
                          
               DEN_reg = 1'b0;
               next_state = (DRDY) ? SET_DI : WAIT_FOR_DRDY;
               end
           SET_DI: begin
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               RST_reg = 1'b1;
               DADDR_reg = CLK1REG_0_ADDR;
               WAIT_LED_reg = 1'b0;
                          
               DI_reg = VAL_TO_WRITE | (MASK & DO);
               next_state = SET_DEN_DWE;
               end
           SET_DEN_DWE: begin
               DI_reg = VAL_TO_WRITE | (MASK & DO);
               RST_reg = 1'b1;
               DADDR_reg = CLK1REG_0_ADDR;
               WAIT_LED_reg = 1'b0;
                          
               DEN_reg = 1'b1;
               DWE_reg = 1'b1;
               next_state = RST_DEN_WAIT_FOR_DRDY;
               end
           RST_DEN_WAIT_FOR_DRDY: begin
               DI_reg = VAL_TO_WRITE | (MASK & DO);
               RST_reg = 1'b1;
               DADDR_reg = CLK1REG_0_ADDR;
               WAIT_LED_reg = 1'b0;
             
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               next_state = (DRDY) ? DEASSERT_RST : RST_DEN_WAIT_FOR_DRDY;
               end
           DEASSERT_RST: begin
               DI_reg = VAL_TO_WRITE | (MASK & DO);
               DADDR_reg = CLK1REG_0_ADDR;       
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               WAIT_LED_reg = 1'b0;
               
               RST_reg = 1'b0;
               next_state = WAIT_FOR_LOCK;
               end
           WAIT_FOR_LOCK: begin
               DI_reg = VAL_TO_WRITE | (MASK & DO);
               RST_reg = 1'b0;
               DADDR_reg = CLK1REG_0_ADDR;
                        
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               next_state = (LOCKED) ? RST_STATE : WAIT_FOR_LOCK; 
               WAIT_LED_reg = 1'b1;
               end
           default: begin
               DI_reg = 16'b0;
               DEN_reg = 1'b0;
               DWE_reg = 1'b0;
               RST_reg = 1'b0;
               DADDR_reg = 7'b0;
               WAIT_LED_reg = 1'b0;
               
               next_state = RST_STATE;
               end
           endcase
	   end


///// End of FSM code ///////////////////////////
	
	wire CLKg;
	BUFG global_buffer_0(.I(CLK), .O(CLKg));
	
	assign LOCKED_out = LOCKED;

	
	MMCME2_ADV #( .BANDWIDTH("OPTIMIZED"), // Jitter programming ("HIGH","LOW","OPTIMIZED") 
        .CLKFBOUT_MULT_F(6.000), // Multiply value for all CLKOUT (2.000-64.000). 
        .CLKFBOUT_PHASE(0.0), // Phase offset in degrees of CLKFB (0.00-360.00). 
        .CLKIN1_PERIOD(10.000), 
        .CLKOUT0_DIVIDE_F(12), 
        .CLKOUT0_DUTY_CYCLE(0.5), 
        .CLKOUT0_PHASE(0.0), 
        .COMPENSATION("ZHOLD"), // "ZHOLD", "INTERNAL", "EXTERNAL" or "BUF_IN" 
        .DIVCLK_DIVIDE(1), // Master division value (1-106)
        .REF_JITTER1(0.0)
    ) MMCME2_ADV_inst ( 
        .CLKOUT0(CLKOUT0), // 1-bit output: CLKOUT0 output 
        .DO(DO), // 16-bit output: DRP data output 
        .DRDY(DRDY), // 1-bit output: DRP ready output 
        .LOCKED(LOCKED), // 1-bit output: LOCK output 
        .CLKIN1(CLKg), // 1-bit input: Primary clock input 
        .RST(RST), // 1-bit input: Reset input 
        .DADDR(DADDR), // 7-bit input: DRP adrress input 
        .DCLK(CLKg), // 1-bit input: DRP clock input 
        .DEN(DEN), // 1-bit input: DRP enable input 
        .DI(DI), // 16-bit input: DRP data input 
        .DWE(DWE), // 1-bit input: DRP write enable input 
        .CLKFBOUT(clkfb_output), 
        .CLKFBIN(clkfb_output) // manually connect feedback loop
    );
    // End of MMCME2_ADV instantiation

    OBUFDS diff_out_buf_0 (.O(CLKout), .OB(CLKoutb), .I(CLKOUT0));
	// User logic ends

	endmodule
