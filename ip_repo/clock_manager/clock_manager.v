`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/16/2016 11:12:04 AM
// Design Name: 
// Module Name: clock_manager
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clock_manager(
        input CLKIN,
        input RST,
        input [6:0] DADDR,
        input DCLK,
        input DEN,
        input [15:0] DRP_DI,
        input DWE,
        input fclk_en,
        output [15:0] DRP_DO,
        output DRDY,
        output CLKOUT1g,
        output CLKOUT2g,
        output CLKOUT3g,
        output CLKOUT4g,
        output CLKOUT5g

    );
    
    wire CLKOUT1, CLKOUT2, CLKOUT3, CLKOUT4, CLKOUT5;
    wire CLKFB;
    
    MMCME2_ADV #(
        .BANDWIDTH("OPTIMIZED"),
        .CLKFBOUT_MULT_F(8.0),
        .CLKFBOUT_PHASE(0.0),
        .CLKIN1_PERIOD(10.0),
        .CLKOUT1_DIVIDE(80),
        .CLKOUT2_DIVIDE(80),
        .CLKOUT3_DIVIDE(80),
        .CLKOUT4_DIVIDE(80),
        .CLKOUT5_DIVIDE(5),
        .CLKOUT1_DUTY_CYCLE(0.5),
        .CLKOUT2_DUTY_CYCLE(0.5),
        .CLKOUT3_DUTY_CYCLE(0.5),
        .CLKOUT4_DUTY_CYCLE(0.5),
        .CLKOUT5_DUTY_CYCLE(0.5),
        .CLKOUT1_PHASE(-90.0),
        .CLKOUT2_PHASE(0.0),
        .CLKOUT3_PHASE(0.0),
        .CLKOUT4_PHASE(45.0),
        .CLKOUT5_PHASE(90.0),
        .CLKOUT4_CASCADE("FALSE"),
        .COMPENSATION("ZHOLD"),
        .DIVCLK_DIVIDE(1),
        .REF_JITTER1(0.0),
        .STARTUP_WAIT("FALSE"),
        .CLKOUT1_USE_FINE_PS("FALSE"),
        .CLKOUT2_USE_FINE_PS("FALSE"),
        .CLKOUT3_USE_FINE_PS("FALSE"),
        .CLKOUT4_USE_FINE_PS("FALSE"),
        .CLKOUT5_USE_FINE_PS("FALSE")
    ) mmcm_0 (
        .CLKOUT1(CLKOUT1),
        .CLKOUT2(CLKOUT2),
        .CLKOUT3(CLKOUT3),
        .CLKOUT4(CLKOUT4),
        .CLKOUT5(CLKOUT5),
        .DO(DRP_DO),
        .DRDY(DRDY),
        .CLKIN1(CLKIN),
        .CLKINSEL(1'b1),
        .PWRDWN(1'b0),
        .RST(RST),
        .DADDR(DADDR),
        .DCLK(DCLK),
        .DEN(DEN),
        .DI(DRP_DI),
        .DWE(DWE),
        .CLKFBIN(CLKFB),
        .CLKFBOUT(CLKFB)
    );
    
    BUFG bufg0(.I(CLKOUT1), .O(CLKOUT1g));
    BUFG bufg1(.I(CLKOUT2), .O(CLKOUT2g));
    BUFG bufg2(.I(CLKOUT3), .O(CLKOUT3g));
    BUFG bufg3(.I(CLKOUT4), .O(CLKOUT4g));
    BUFGCE bufg4(.I(CLKOUT5), .O(CLKOUT5g), .CE(fclk_en));
endmodule
