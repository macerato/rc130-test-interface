
set_property IOSTANDARD LVDS_25 [get_ports D_p]
set_property PACKAGE_PIN E17 [get_ports D_p]
set_property PACKAGE_PIN N18 [get_ports trigger]
set_property IOSTANDARD LVCMOS33 [get_ports trigger]

set_property IOSTANDARD LVDS_25 [get_ports fast_clk_p]
set_property PACKAGE_PIN L19 [get_ports fast_clk_p]

create_clock -period 6.250 -name VIRTUAL_clk_out1_design_1_clk_wiz_0_0 -waveform {0.000 3.125}
create_clock -period 25.000 -name VIRTUAL_clk_out3_design_1_clk_wiz_0_0 -waveform {0.000 12.500}
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -clock_fall -min -add_delay 1.600 [get_ports D_n]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -clock_fall -max -add_delay 1.700 [get_ports D_n]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -min -add_delay 1.600 [get_ports D_n]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -max -add_delay 1.700 [get_ports D_n]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -clock_fall -min -add_delay 1.600 [get_ports D_p]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -clock_fall -max -add_delay 1.700 [get_ports D_p]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -min -add_delay 1.600 [get_ports D_p]
set_input_delay -clock [get_clocks VIRTUAL_clk_out1_design_1_clk_wiz_0_0] -max -add_delay 1.700 [get_ports D_p]
set_input_delay -clock [get_clocks VIRTUAL_clk_out3_design_1_clk_wiz_0_0] -min -add_delay 0.100 [get_ports trigger]
set_input_delay -clock [get_clocks VIRTUAL_clk_out3_design_1_clk_wiz_0_0] -max -add_delay 5.000 [get_ports trigger]



set_property PACKAGE_PIN G20 [get_ports reset]
set_property IOSTANDARD LVCMOS25 [get_ports reset]


set_property PACKAGE_PIN U15 [get_ports full]
set_property PACKAGE_PIN U18 [get_ports match_out]
set_property PACKAGE_PIN U7 [get_ports match_out_1]
set_property IOSTANDARD LVCMOS33 [get_ports match_out]
set_property IOSTANDARD LVCMOS33 [get_ports full]
set_property IOSTANDARD LVCMOS33 [get_ports match_out_1]

set_property PACKAGE_PIN U19 [get_ports locked]
set_property IOSTANDARD LVCMOS33 [get_ports locked]

set_property PACKAGE_PIN H15 [get_ports slow_data_p]
set_property IOSTANDARD LVDS_25 [get_ports slow_data_p]

set_property PACKAGE_PIN K14 [get_ports slow_clk_p]
set_property IOSTANDARD LVDS_25 [get_ports slow_clk_p]






set_input_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -min -add_delay 0.200 [get_ports trigger]
set_input_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -max -add_delay 4.000 [get_ports trigger]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -min -add_delay 0.100 [get_ports full]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -max -add_delay 0.200 [get_ports full]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -min -add_delay -4.200 [get_ports match_out]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -max -add_delay -3.200 [get_ports match_out]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -min -add_delay -4.200 [get_ports match_out_1]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -max -add_delay -3.200 [get_ports match_out_1]
set_output_delay -clock [get_clocks clk_fpga_0] -min -add_delay 0.100 [get_ports slow_data_n]
set_output_delay -clock [get_clocks clk_fpga_0] -max -add_delay 0.200 [get_ports slow_data_n]
set_output_delay -clock [get_clocks clk_fpga_0] -min -add_delay 0.100 [get_ports slow_data_p]
set_output_delay -clock [get_clocks clk_fpga_0] -max -add_delay 0.200 [get_ports slow_data_p]




set_property PACKAGE_PIN U14 [get_ports stop_out]
set_property IOSTANDARD LVCMOS33 [get_ports stop_out]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -min -add_delay -4.200 [get_ports stop_out]
set_output_delay -clock [get_clocks design_1_i/clk_wiz_0/inst/clk_in1] -max -add_delay -3.200 [get_ports stop_out]

set_property PACKAGE_PIN G19 [get_ports trigger_button]
set_property IOSTANDARD LVCMOS25 [get_ports trigger_button]
