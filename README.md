he RC130 Test Platform is a MicroZed-7020 based design for interfacing the RC130 Repeater Chip and verifying its functionality. It provides an interface for writing and reading registers on the RC130, issuing commands, and reading back memory at 640 MHz. The interface is implemented as a terminal written in C which runs on the MicroZed processor.

See the documentation in the root directory for more information.

### Setup ###

Clone the project to some local directory. Then execute the build.bat script (or alternatively, just invoke Vivado to execute build.tcl). This will create a Vivado project folder which contains the project file. Open the block diagram, then generate the bitstream. Once the hardware is exported to SDK, create a BSP called standalone_bsp_0 and an application project using that BSP. Copy the source code in the SDK into the application project, and then the application is ready to execute on the MicroZed.

As of now, the timing-fix branch is the most current. It needs to be merged into master.