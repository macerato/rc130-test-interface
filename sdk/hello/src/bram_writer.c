#include "xil_io.h"
#include "xil_types.h"
#include "bram_writer.h"
#include "sleep.h"
#include "constants.h"

u32 regLastWrite[4] = {0x40, 0x0A, 0x52, 0x94}; // 400A5294

/**
 * Load the bram
 */
void enable_serialout()
{
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000001);
	usleep(1000);
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000000);
}

/**
 * Send a command by appending the appropriate header and trailer and writing
 * to the bram.
 */
void short_command(u32 payload)
{
	u32 cmd = (0xC0) | (payload << 1);
	Xil_Out32(XPAR_BRAM_0_BASEADDR, cmd);
	enable_serialout();
	Xil_Out32(XPAR_BRAM_0_BASEADDR, 0x0);
}

/**
 * Write a 40 bit command to the BRAM
 */
void command_40(u32 type, u32 payload)
{
	u64 cmd;
	u64 type64 = (u64) type;
	u64 payload64 = (u64) payload;
	u64 header = (u64) 0xC0UL;
	cmd = (header << 32) | (type64 << 33) | (payload64 << 1);

	int i = 0;
	while(i < 5)
	{
		u8 byte = (cmd >> 8*(4 - i));
		Xil_Out32(XPAR_BRAM_0_BASEADDR + 4*i, byte);
		i++;
	}
	enable_serialout();

	regLastWrite[type - 1] = payload;

	// Clear the bram *******
	i = 0;
	while(i < 5)
	{
		Xil_Out32(XPAR_BRAM_0_BASEADDR + 4*i, 0x0);
		i++;
	}
}

/**
 * Reset the chip according to the given code
 */
void reset(u32 rst)
{
	u32 cmd = RESET_3;
	switch(rst)
	{
	case 1: cmd = RESET_1;
	case 2: cmd = RESET_2;
	case 3: cmd = RESET_3;
	default: cmd = RESET_3;
	}
	short_command(cmd);
}

/**
 * Configure the given driver
 *
 * driver:		   Driver to enable, between 0 and 3
 * on:			   Enables the driver, either 0 or 1
 * preEm:		   Turns on the pre-emphasis (0 or 1)
 * currentSetting: Current value, can be between 0 and 7. Consult documentation for mA vals.
 */
void cfg_driver(u32 driver, u32 on, u32 currentSetting, u32 preEm)
{
	if(driver < 0  ||  driver > 3)
	{
		xil_printf("Invalid driver number.\n\r");
	}
	else if(currentSetting < 0 || currentSetting > 7)
	{
		xil_printf("Invalid current setting\n\r");
	}
	else if(!((preEm == 0) || (preEm == 1)))
	{
		xil_printf("Invalid pre-emphasis setting\n\r");
	}
	else if(!((on == 0) || (on == 1)))
	{
		xil_printf("Invalid enable setting\n\r");
	}
	else
	{
		u32 payload = (((on << 4) | (preEm << 3) | currentSetting) << driver*5) | (0xC << 28);
		u32 filteredPrevPayload = regLastWrite[0] & ~((0xC << 28) | (0x1F << driver*5));
		payload = payload | filteredPrevPayload;

		command_40(0x1, payload);
		xil_printf("Driver %i enabled.\n\r", driver);
	}
}

/**
 * Completely disable all drivers at once
 */
void disable_all()
{
	u32 payload = regLastWrite[0] & 0xFFF00000;
	command_40(0x1, payload);
	xil_printf("All drivers disabled.\n\r");
}

/**
 * Determine whether the chip should use the software settings for the drivers or the
 * jumper override on the board
 */
void cfg_drv_jmp(u32 on)
{
	u32 payload = (regLastWrite[0] & 0xBFFFFFFF) | (((0x4 | (on << 1)) << 28));
	command_40(0x1, payload);
	if(on == 1)
	{
		xil_printf("The RC130 will now use the software settings for the drivers.\n\r");
	}
	else if(on == 0)
	{
		xil_printf("The RC130 will now use the jumper on the board for driver settings.\n\r");
	}
}
/**
 * Write to the memory
 */
void writeMemory(u32 memory[], u32 size, u8 startAddr)
{

	// Check that size of mem = 64*n for an integer n
	if(sizeof(memory) % 2*sizeof(u32) != 0)
	{
		xil_printf("Not an integer multiple of 64 bits.\n\r");
		return;
	}
	u32 n = size;

	xil_printf("Writing %i 64 bit line(s) of memory...\n\r", n/2);

	setWritePointer(startAddr);
	int i = 0;
	while(i < n)
	{
		xil_printf("%i\n\r", i);

		command_40(0x0, memory[i]);
		xil_printf("Writing %x\n\r", memory[i]);

		i++;
	}
}

/*
 * write and config read memory
 */
void wrmem(u32 memory[], u32 size, u8 startAddr, u8 aStart, u8 nData, u8 nRepeat)
{
	writeMemory(memory, size, startAddr);
	enableMemoryRead(aStart, nData, nRepeat);
}

/**
 * Set the write pointer
 */
void setWritePointer(u8 startAddr)
{
	// Write to register 3 to configure initial location of write pointer in RAM
	command_40(0x3, startAddr);
	xil_printf("Set the write pointer to %x\n\r", startAddr);
}

/**
 * Enable the memory output from the chip
 */
void enableMemoryRead(u8 aStart, u32 nData, u32 nRepeat)
{
	// Configre readout settings
	u32 payload = (nRepeat << 16) | (nData << 8) | aStart;
	command_40(0x2, payload);

	// Enable fast clock receiver and output driver with default current settings
	command_40(0x4, 0x34);

	// Enable transmit
}

/**
 * Begin transmit
 */
void enableTransmit()
{
	short_command(0x5);
	//fclk_enable(1);
	//usleep(1000);
	//fclk_enable(0);
}

void fclk_enable(int en)
{
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, (en << 1) & 0x2);
}

/**
 * disable memory output
 */
void disableMemoryRead()
{
	short_command(0x6);
	xil_printf("Memory read disabled.\n\r");
}
