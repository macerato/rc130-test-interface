/*
 * utility.h
 *
 *  Created on: Jun 10, 2016
 *      Author: macerato
 */

#ifndef UTILITY_H_
#define UTILITY_H_
#include "xil_types.h"

char** split_str(char*, const char);

#endif /* UTILITY_H_ */
