/*
 * constants.h
 *
 *  Created on: Jun 10, 2016
 *      Author: Mark Macerato
 */

#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define LOAD_MEMORY 	0x0;
#define REG_1_WRITE 	0x1;
#define REG_2_WRITE 	0x2;
#define REG_3_WRITE 	0x3;
#define REG_4_WRITE 	0x4;

#define START_PATTERN	0x5;
#define STOP_PATTERN	0x6;
#define RESET_1			0x7;

#define READ_MEMORY		0x8;
#define REG_1_READ		0x9;
#define REG_2_READ		0xA;
#define REG_3_READ		0xB;
#define CONT_PATTERN	0xC;

#define	RESET_2			0xD;
#define RESET_3			0xE;

#endif /* CONSTANTS_H_ */
