/*
 * Parser.c
 *
 * This application will parse terminal input to the Zynq 7000 and interpret as commands to issue to the
 * RC130 repeater chip.
 *
 * See commands.txt for terminal documentation
 *
 *  Created on: Jun 10, 2016
 *      Author: Mark Macerato
 */

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "xil_types.h"
#include "constants.h"
#include "utility.h"
#include "sleep.h"
#include "xil_io.h"
#include "Parser.h"
#include "bram_writer.h"
#include "stdio.h"

// number of consecutive fast data errors before terminal stops
// recording them
u32 MAX_ERRORS = 1000;

/*
 * Take an input string and determine what command should be executed
 */
int parse(char* input)
{
	char** words = split_str(input, ' ');

	char* cmd = strtok(words[0], "\n\r");
	if(strcmp(cmd, "rreg") == 0)
	{
		rreg((u32)strtol(words[1], NULL, 16));
	}
	else if(strcmp(cmd, "transmit") == 0)
	{
		enableTransmit();
	}
	else if(strcmp(cmd, "empty") == 0)
	{
		xil_printf("%08x\n\r", empty());
	}
	else if(strcmp(cmd, "monitor") == 0)
	{
		monitor();
	}
	else if(strcmp(cmd, "rdfifo") == 0)
	{
		read_fifo();
	}
	else if(strcmp(cmd, "cmd") == 0)
	{
		short_command(strtol(words[1], NULL, 16));
	}
	else if(strcmp(cmd, "help") == 0)
	{
		xil_printf(help());
	}
	else if(strcmp(cmd, "wreg") == 0)
	{
		wreg((u32)strtol(words[1], NULL, 16), (u32)strtoul(words[2], NULL, 16));
	}
	else if(strcmp(cmd, "quit") == 0)
	{
		xil_printf("\n\rDone.\n\r");
		return 1;
	}
	else if(strcmp(cmd, "reset") == 0)
	{
		reset(strtol(words[1], NULL, 10));
		xil_printf("Reset issued.\n\r");
	}
	else if((strcmp(cmd, "drv") == 0))
	{
		cfg_driver(strtol(words[1], NULL, 10), strtol(words[2], NULL, 10), strtol(words[3], NULL, 10), strtol(words[4], NULL, 10));
	}
	else if((strcmp(cmd, "disdrvall") == 0))
	{
		disable_all();
	}
	else if((strcmp(cmd, "cfgjmp") == 0))
	{
		cfg_drv_jmp(strtol(words[1], NULL, 10));
	}
	else if((strcmp(cmd, "ldmem") == 0))
	{
		ldmem();
	}
	else if((strcmp(cmd, "setptr") == 0))
	{
		setWritePointer(strtol(words[1], NULL, 16));
	}
	else if((strcmp(cmd, "rmem") == 0))
	{
		rmem(strtol(words[1], NULL, 16), strtol(words[2], NULL, 10),strtol(words[3], NULL, 10));
	}
	else if((strcmp(cmd, "stopmem") == 0))
	{
		disableMemoryRead();
	}
	else
	{
		xil_printf("Unrecognized command. Type 'help' for a list of commands.\n\r");
	}

	return 0;
}

/**
 * Read out a register
 *
 * reg: the register to read
 */
void rreg(u32 reg)
{
	if(reg < 0 || reg > 4)
	{
		xil_printf("Not a valid register numner\n\r");
		return;
	}

	u32 cmd;

	if(reg == 0)
	{
		cmd = 0xF;
	}
	else
	{
		cmd = reg + 0x8;
	}
	short_command(cmd);
	xil_printf("Register read issued\n\r");
}

/**
 * Write to a register
 */
void wreg(u32 reg, u32 payload)
{
	if(!(reg >= 0 && reg <= 4))
	{
		xil_printf("Not a valid register numner\n\r");
		return;
	}
	command_40(reg, payload);
	xil_printf("Wrote %x to register %i\n\r", payload, reg);
}

/**
 * Print the help list
 */
char* help()
{
	char* help = "\n\rList of commands:\n\r'rreg n'\tRead register n\n\r'wreg n val'\tWrite val to register n\n\r'quit'\t\tExit terminal\n\r";
	return help;
}

/**
 * load the memory of the RC130 with a pattern
 */
void ldmem()
{
	//u32 mem[2] = {0xFFFFFFFF, 0xFFFFFFFF};
	//u32 mem[4] = {0x11111111, 0x11111111, 0xFFFFFFFF, 0xFFFFFFFF};
	//u32 mem[10] = {0x11111111, 0x11111111, 0x33333333, 0x33333333, 0x77777777, 0x77777777 , 0xFFFFFFFF, 0xFFFFFFFF, 0xAAAAAAAA, 0xAAAAAAAA};
	//u32 mem[2] = {0xF10710AB, 0x1FF0ABB1};

	//u32 mem[4] = {0x2d5cbade, 0xadbeef1c, 0x2d5cbade, 0xadbeef1c};
	u32 mem[2*128];

	int i = 0;
	while(i < 2*128)
	{
		mem[i] = 0x2d5cbade;
		mem[i+1] = 0xadbeef1c;
		//mem[i] = 0xF7F0807E;
		//mem[i] = 0xDEADBEEF;
		//i = i + 1;
		i = i + 2;
	}

	//xil_printf("Loaded.\n\r");
	u32 size = sizeof(mem)/sizeof(mem[0]);
	writeMemory(mem, size, 0x0);
	xil_printf("Memory written.\n\r");
}


/**
 * Begin the memory readout
 */
void rmem(u32 startAddr, u32 nData, u32 nRepeat)
{
	enableMemoryRead(startAddr, nData, nRepeat);
	xil_printf("Memory read enabled.\n\r");
}

// read out one line of the fifo
void read_fifo()
{
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000002);
	u32 reg1 = Xil_In32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR + 8);
	u32 reg2 = Xil_In32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR + 4);
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000000);
	xil_printf("%08x", reg2);
	xil_printf("%08x\n\r", reg1);
}

// determine if the fifo is empty
u32 empty()
{
	u32 empty = Xil_In32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR + 12);
	return empty;
}

// monitor the fifo for errors and print them
void monitor()
{
	u32 errors = 0;
	xil_printf("Running...\n\r");
	while(1)
	{
		if(!empty())
		{
			read_fifo();
			errors++;
			usleep(1000);
			if(errors > MAX_ERRORS)
			{
				xil_printf("Maximum error count reached, halting data collection on software side.");
				errors = 0;
				return;
			}
		}
	}
	xil_printf("...Done.\n\r");
}


