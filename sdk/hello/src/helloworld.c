/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */


///////////////// TERMINAL USE/////////////
///// to load the memory and monitor the readout...
////  wreg 3 0x0
////  ldmem
////  wreg 2 0x8000
////  wreg 4 0x37
////  transmit
////  monitor

///  to stop transmission, cmd 0x5
/////////////////////////////////////////////////

#include <stdio.h>
#include "platform.h"
#include "xil_io.h"
#include "xil_types.h"
#include "Parser.h"
#include "bram_writer.h"
#include "sleep.h"

void write_command(u32, u32);
void disable_all_drivers();
void clock_config();
void enable_driver(u32);


u32 writePointer = XPAR_BRAM_0_BASEADDR;

// bram addr XPAR_AXI_BRAM_CTRL_0_S_AXI_BAwSEADDR

int main()
{
    init_platform();

    // clear the control register
    Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000000);

    // startup message
    xil_printf("RC130 Interface Terminal Starting...\n\rType 'help' for command list\n\r");

    // run the terminal, see parser.c
    char* input = "";
    int res = 0;
    do
    {
    	gets(input);
    	res = parse(input);
    }while(res == 0);


    cleanup_platform();

    return 0;
}
