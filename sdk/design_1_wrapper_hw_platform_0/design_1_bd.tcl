
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2015.3
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7z020clg400-1
#    set_property BOARD_PART em.avnet.com:microzed_7020:part0:1.0 [current_project]

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}



# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "ERROR: Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      puts "INFO: Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   puts "INFO: Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES: 
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   puts "INFO: Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################



# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  # Create ports
  set D_n [ create_bd_port -dir I D_n ]
  set D_p [ create_bd_port -dir I D_p ]
  set fast_clk_n [ create_bd_port -dir O fast_clk_n ]
  set fast_clk_p [ create_bd_port -dir O fast_clk_p ]
  set full [ create_bd_port -dir O full ]
  set locked [ create_bd_port -dir O locked ]
  set match_out [ create_bd_port -dir O match_out ]
  set match_out_1 [ create_bd_port -dir O match_out_1 ]
  set reset [ create_bd_port -dir I reset ]
  set slow_clk_n [ create_bd_port -dir O slow_clk_n ]
  set slow_clk_p [ create_bd_port -dir O slow_clk_p ]
  set slow_data_n [ create_bd_port -dir O slow_data_n ]
  set slow_data_p [ create_bd_port -dir O slow_data_p ]
  set stop_out [ create_bd_port -dir O stop_out ]
  set trigger [ create_bd_port -dir I trigger ]
  set trigger_button [ create_bd_port -dir I trigger_button ]

  # Create instance: axi_bram_ctrl_0, and set properties
  set axi_bram_ctrl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.0 axi_bram_ctrl_0 ]
  set_property -dict [ list \
CONFIG.ECC_TYPE {0} \
CONFIG.PROTOCOL {AXI4LITE} \
CONFIG.SINGLE_PORT_BRAM {1} \
CONFIG.SUPPORTS_NARROW_BURST {0} \
 ] $axi_bram_ctrl_0

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.2 clk_wiz_0 ]
  set_property -dict [ list \
CONFIG.CLKIN1_JITTER_PS {125.0} \
CONFIG.CLKOUT1_JITTER {134.360} \
CONFIG.CLKOUT1_PHASE_ERROR {135.911} \
CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {320} \
CONFIG.CLKOUT1_REQUESTED_PHASE {-180} \
CONFIG.CLKOUT2_DRIVES {No_buffer} \
CONFIG.CLKOUT2_JITTER {118.382} \
CONFIG.CLKOUT2_PHASE_ERROR {135.911} \
CONFIG.CLKOUT2_REQUESTED_OUT_FREQ {640} \
CONFIG.CLKOUT2_REQUESTED_PHASE {0.0} \
CONFIG.CLKOUT2_USED {true} \
CONFIG.CLKOUT3_JITTER {175.674} \
CONFIG.CLKOUT3_PHASE_ERROR {135.911} \
CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {80} \
CONFIG.CLKOUT3_USED {true} \
CONFIG.CLKOUT4_JITTER {266.321} \
CONFIG.CLKOUT4_PHASE_ERROR {135.911} \
CONFIG.CLKOUT4_REQUESTED_OUT_FREQ {10.000} \
CONFIG.CLKOUT4_REQUESTED_PHASE {45} \
CONFIG.CLKOUT4_USED {true} \
CONFIG.CLKOUT5_JITTER {134.360} \
CONFIG.CLKOUT5_PHASE_ERROR {135.911} \
CONFIG.CLKOUT5_REQUESTED_OUT_FREQ {320} \
CONFIG.CLKOUT5_REQUESTED_PHASE {180} \
CONFIG.CLKOUT5_USED {true} \
CONFIG.MMCM_CLKFBOUT_MULT_F {8.000} \
CONFIG.MMCM_CLKIN1_PERIOD {12.5} \
CONFIG.MMCM_CLKOUT0_DIVIDE_F {2.000} \
CONFIG.MMCM_CLKOUT0_PHASE {-180.000} \
CONFIG.MMCM_CLKOUT1_DIVIDE {1} \
CONFIG.MMCM_CLKOUT1_PHASE {0.000} \
CONFIG.MMCM_CLKOUT2_DIVIDE {8} \
CONFIG.MMCM_CLKOUT3_DIVIDE {64} \
CONFIG.MMCM_CLKOUT3_PHASE {45.000} \
CONFIG.MMCM_CLKOUT4_DIVIDE {2} \
CONFIG.MMCM_CLKOUT4_PHASE {180.000} \
CONFIG.MMCM_DIVCLK_DIVIDE {1} \
CONFIG.NUM_OUT_CLKS {5} \
CONFIG.PRIM_IN_FREQ {80.000} \
CONFIG.USE_CLKFB_STOPPED {false} \
 ] $clk_wiz_0

  # Create instance: control_registers_v1_0_0, and set properties
  set control_registers_v1_0_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:control_registers_v1_0:1.0 control_registers_v1_0_0 ]

  # Create instance: fast_data_0, and set properties
  set fast_data_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:fast_data:1.0 fast_data_0 ]

  # Create instance: lvds_buff_0, and set properties
  set lvds_buff_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:lvds_buff:1.0 lvds_buff_0 ]

  # Create instance: proc_sys_reset_0, and set properties
  set proc_sys_reset_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_0 ]
  set_property -dict [ list \
CONFIG.C_AUX_RESET_HIGH {1} \
 ] $proc_sys_reset_0

  # Create instance: proc_sys_reset_1, and set properties
  set proc_sys_reset_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_1 ]
  set_property -dict [ list \
CONFIG.C_AUX_RESET_HIGH {1} \
 ] $proc_sys_reset_1

  # Create instance: processing_system7_0, and set properties
  set processing_system7_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7:5.5 processing_system7_0 ]
  set_property -dict [ list \
CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {667} \
CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
CONFIG.PCW_ENET0_ENET0_IO {<Select>} \
CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {0} \
CONFIG.PCW_ENET0_GRP_MDIO_IO {<Select>} \
CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {0} \
CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {1000 Mbps} \
CONFIG.PCW_ENET0_RESET_ENABLE {0} \
CONFIG.PCW_EN_CLK0_PORT {1} \
CONFIG.PCW_EN_CLK1_PORT {0} \
CONFIG.PCW_EN_CLK2_PORT {0} \
CONFIG.PCW_EN_CLK3_PORT {0} \
CONFIG.PCW_EN_DDR {1} \
CONFIG.PCW_EN_RST0_PORT {1} \
CONFIG.PCW_EN_RST1_PORT {0} \
CONFIG.PCW_EN_RST2_PORT {0} \
CONFIG.PCW_EN_RST3_PORT {0} \
CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_FCLK_CLK0_BUF {true} \
CONFIG.PCW_FCLK_CLK1_BUF {false} \
CONFIG.PCW_FCLK_CLK2_BUF {false} \
CONFIG.PCW_FCLK_CLK3_BUF {false} \
CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {80.000} \
CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {100} \
CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {33.333333} \
CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {0} \
CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_I2C_RESET_ENABLE {0} \
CONFIG.PCW_MIO_0_PULLUP {disabled} \
CONFIG.PCW_MIO_0_SLEW {slow} \
CONFIG.PCW_MIO_10_PULLUP {disabled} \
CONFIG.PCW_MIO_10_SLEW {slow} \
CONFIG.PCW_MIO_11_PULLUP {disabled} \
CONFIG.PCW_MIO_11_SLEW {slow} \
CONFIG.PCW_MIO_12_PULLUP {disabled} \
CONFIG.PCW_MIO_12_SLEW {slow} \
CONFIG.PCW_MIO_13_PULLUP {disabled} \
CONFIG.PCW_MIO_13_SLEW {slow} \
CONFIG.PCW_MIO_14_PULLUP {disabled} \
CONFIG.PCW_MIO_14_SLEW {slow} \
CONFIG.PCW_MIO_15_PULLUP {disabled} \
CONFIG.PCW_MIO_15_SLEW {slow} \
CONFIG.PCW_MIO_16_PULLUP {disabled} \
CONFIG.PCW_MIO_16_SLEW {slow} \
CONFIG.PCW_MIO_17_PULLUP {disabled} \
CONFIG.PCW_MIO_17_SLEW {slow} \
CONFIG.PCW_MIO_18_PULLUP {disabled} \
CONFIG.PCW_MIO_18_SLEW {slow} \
CONFIG.PCW_MIO_19_PULLUP {disabled} \
CONFIG.PCW_MIO_19_SLEW {slow} \
CONFIG.PCW_MIO_1_PULLUP {disabled} \
CONFIG.PCW_MIO_1_SLEW {slow} \
CONFIG.PCW_MIO_20_PULLUP {disabled} \
CONFIG.PCW_MIO_20_SLEW {slow} \
CONFIG.PCW_MIO_21_PULLUP {disabled} \
CONFIG.PCW_MIO_21_SLEW {slow} \
CONFIG.PCW_MIO_22_PULLUP {disabled} \
CONFIG.PCW_MIO_22_SLEW {slow} \
CONFIG.PCW_MIO_23_PULLUP {disabled} \
CONFIG.PCW_MIO_23_SLEW {slow} \
CONFIG.PCW_MIO_24_PULLUP {disabled} \
CONFIG.PCW_MIO_24_SLEW {slow} \
CONFIG.PCW_MIO_25_PULLUP {disabled} \
CONFIG.PCW_MIO_25_SLEW {slow} \
CONFIG.PCW_MIO_26_PULLUP {disabled} \
CONFIG.PCW_MIO_26_SLEW {slow} \
CONFIG.PCW_MIO_27_PULLUP {disabled} \
CONFIG.PCW_MIO_27_SLEW {slow} \
CONFIG.PCW_MIO_28_PULLUP {disabled} \
CONFIG.PCW_MIO_28_SLEW {slow} \
CONFIG.PCW_MIO_29_PULLUP {disabled} \
CONFIG.PCW_MIO_29_SLEW {slow} \
CONFIG.PCW_MIO_2_PULLUP {disabled} \
CONFIG.PCW_MIO_2_SLEW {slow} \
CONFIG.PCW_MIO_30_PULLUP {disabled} \
CONFIG.PCW_MIO_30_SLEW {slow} \
CONFIG.PCW_MIO_31_PULLUP {disabled} \
CONFIG.PCW_MIO_31_SLEW {slow} \
CONFIG.PCW_MIO_32_PULLUP {disabled} \
CONFIG.PCW_MIO_32_SLEW {slow} \
CONFIG.PCW_MIO_33_PULLUP {disabled} \
CONFIG.PCW_MIO_33_SLEW {slow} \
CONFIG.PCW_MIO_34_PULLUP {disabled} \
CONFIG.PCW_MIO_34_SLEW {slow} \
CONFIG.PCW_MIO_35_PULLUP {disabled} \
CONFIG.PCW_MIO_35_SLEW {slow} \
CONFIG.PCW_MIO_36_PULLUP {disabled} \
CONFIG.PCW_MIO_36_SLEW {slow} \
CONFIG.PCW_MIO_37_PULLUP {disabled} \
CONFIG.PCW_MIO_37_SLEW {slow} \
CONFIG.PCW_MIO_38_PULLUP {disabled} \
CONFIG.PCW_MIO_38_SLEW {slow} \
CONFIG.PCW_MIO_39_PULLUP {disabled} \
CONFIG.PCW_MIO_39_SLEW {slow} \
CONFIG.PCW_MIO_3_PULLUP {disabled} \
CONFIG.PCW_MIO_3_SLEW {slow} \
CONFIG.PCW_MIO_40_PULLUP {disabled} \
CONFIG.PCW_MIO_40_SLEW {slow} \
CONFIG.PCW_MIO_41_PULLUP {disabled} \
CONFIG.PCW_MIO_41_SLEW {slow} \
CONFIG.PCW_MIO_42_PULLUP {disabled} \
CONFIG.PCW_MIO_42_SLEW {slow} \
CONFIG.PCW_MIO_43_PULLUP {disabled} \
CONFIG.PCW_MIO_43_SLEW {slow} \
CONFIG.PCW_MIO_44_PULLUP {disabled} \
CONFIG.PCW_MIO_44_SLEW {slow} \
CONFIG.PCW_MIO_45_PULLUP {disabled} \
CONFIG.PCW_MIO_45_SLEW {slow} \
CONFIG.PCW_MIO_46_PULLUP {disabled} \
CONFIG.PCW_MIO_46_SLEW {slow} \
CONFIG.PCW_MIO_47_PULLUP {disabled} \
CONFIG.PCW_MIO_47_SLEW {slow} \
CONFIG.PCW_MIO_48_PULLUP {disabled} \
CONFIG.PCW_MIO_48_SLEW {slow} \
CONFIG.PCW_MIO_49_PULLUP {disabled} \
CONFIG.PCW_MIO_49_SLEW {slow} \
CONFIG.PCW_MIO_4_PULLUP {disabled} \
CONFIG.PCW_MIO_4_SLEW {slow} \
CONFIG.PCW_MIO_50_PULLUP {disabled} \
CONFIG.PCW_MIO_50_SLEW {slow} \
CONFIG.PCW_MIO_51_PULLUP {disabled} \
CONFIG.PCW_MIO_51_SLEW {slow} \
CONFIG.PCW_MIO_52_PULLUP {disabled} \
CONFIG.PCW_MIO_52_SLEW {slow} \
CONFIG.PCW_MIO_53_PULLUP {disabled} \
CONFIG.PCW_MIO_53_SLEW {slow} \
CONFIG.PCW_MIO_5_PULLUP {disabled} \
CONFIG.PCW_MIO_5_SLEW {slow} \
CONFIG.PCW_MIO_6_PULLUP {disabled} \
CONFIG.PCW_MIO_6_SLEW {slow} \
CONFIG.PCW_MIO_7_PULLUP {disabled} \
CONFIG.PCW_MIO_7_SLEW {slow} \
CONFIG.PCW_MIO_8_PULLUP {disabled} \
CONFIG.PCW_MIO_8_SLEW {slow} \
CONFIG.PCW_MIO_9_PULLUP {disabled} \
CONFIG.PCW_MIO_9_SLEW {slow} \
CONFIG.PCW_PACKAGE_NAME {clg400} \
CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {1} \
CONFIG.PCW_QSPI_GRP_FBCLK_IO {MIO 8} \
CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
CONFIG.PCW_SD0_GRP_CD_ENABLE {1} \
CONFIG.PCW_SD0_GRP_CD_IO {MIO 46} \
CONFIG.PCW_SD0_GRP_WP_ENABLE {1} \
CONFIG.PCW_SD0_GRP_WP_IO {MIO 50} \
CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {25} \
CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_UART1_UART1_IO {MIO 48 .. 49} \
CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {50} \
CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
CONFIG.PCW_UIPARAM_DDR_BL {8} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.294} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.298} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.338} \
CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.334} \
CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {32 Bit} \
CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {39.7} \
CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {39.7} \
CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {54.14} \
CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {54.14} \
CONFIG.PCW_UIPARAM_DDR_CWL {6} \
CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {4096 MBits} \
CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {50.05} \
CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {50.43} \
CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {50.10} \
CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {50.01} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {-0.073} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {-0.072} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.024} \
CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.023} \
CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {49.59} \
CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {51.74} \
CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {50.32} \
CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {48.55} \
CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3 (Low Voltage)} \
CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41K256M16 RE-125} \
CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
CONFIG.PCW_UIPARAM_DDR_T_FAW {40.0} \
CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {35.0} \
CONFIG.PCW_UIPARAM_DDR_T_RC {48.75} \
CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {1} \
CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
CONFIG.PCW_USB0_RESET_ENABLE {1} \
CONFIG.PCW_USB0_RESET_IO {MIO 7} \
CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
CONFIG.PCW_USE_M_AXI_GP0 {1} \
CONFIG.PCW_USE_M_AXI_GP1 {0} \
 ] $processing_system7_0

  # Create instance: processing_system7_0_axi_periph, and set properties
  set processing_system7_0_axi_periph [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect:2.1 processing_system7_0_axi_periph ]
  set_property -dict [ list \
CONFIG.NUM_MI {2} \
 ] $processing_system7_0_axi_periph

  # Create instance: rst_processing_system7_0_80M, and set properties
  set rst_processing_system7_0_80M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_processing_system7_0_80M ]
  set_property -dict [ list \
CONFIG.C_AUX_RESET_HIGH {1} \
 ] $rst_processing_system7_0_80M

  # Create instance: slow_data_0, and set properties
  set slow_data_0 [ create_bd_cell -type ip -vlnv xilinx.com:user:slow_data:1.0 slow_data_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net processing_system7_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins processing_system7_0/DDR]
  connect_bd_intf_net -intf_net processing_system7_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins processing_system7_0/FIXED_IO]
  connect_bd_intf_net -intf_net processing_system7_0_M_AXI_GP0 [get_bd_intf_pins processing_system7_0/M_AXI_GP0] [get_bd_intf_pins processing_system7_0_axi_periph/S00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M00_AXI [get_bd_intf_pins control_registers_v1_0_0/s00_axi] [get_bd_intf_pins processing_system7_0_axi_periph/M00_AXI]
  connect_bd_intf_net -intf_net processing_system7_0_axi_periph_M01_AXI [get_bd_intf_pins axi_bram_ctrl_0/S_AXI] [get_bd_intf_pins processing_system7_0_axi_periph/M01_AXI]

  # Create port connections
  connect_bd_net -net D_n_1 [get_bd_ports D_n] [get_bd_pins fast_data_0/D_n]
  connect_bd_net -net D_p_1 [get_bd_ports D_p] [get_bd_pins fast_data_0/D_p]
  connect_bd_net -net axi_bram_ctrl_0_bram_addr_a [get_bd_pins axi_bram_ctrl_0/bram_addr_a] [get_bd_pins slow_data_0/ADDRB]
  connect_bd_net -net axi_bram_ctrl_0_bram_clk_a [get_bd_pins axi_bram_ctrl_0/bram_clk_a] [get_bd_pins slow_data_0/CLKB]
  connect_bd_net -net axi_bram_ctrl_0_bram_en_a [get_bd_pins axi_bram_ctrl_0/bram_en_a] [get_bd_pins slow_data_0/ENB]
  connect_bd_net -net axi_bram_ctrl_0_bram_rst_a [get_bd_pins axi_bram_ctrl_0/bram_rst_a] [get_bd_pins slow_data_0/RSTB]
  connect_bd_net -net axi_bram_ctrl_0_bram_we_a [get_bd_pins axi_bram_ctrl_0/bram_we_a] [get_bd_pins slow_data_0/WEB]
  connect_bd_net -net axi_bram_ctrl_0_bram_wrdata_a [get_bd_pins axi_bram_ctrl_0/bram_wrdata_a] [get_bd_pins slow_data_0/DIB]
  connect_bd_net -net clk_wiz_0_clk_out1 [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins fast_data_0/clk]
  connect_bd_net -net clk_wiz_0_clk_out2 [get_bd_pins clk_wiz_0/clk_out2] [get_bd_pins fast_data_0/fast_clk]
  connect_bd_net -net clk_wiz_0_clk_out3 [get_bd_pins clk_wiz_0/clk_out3] [get_bd_pins fast_data_0/clkdiv] [get_bd_pins proc_sys_reset_0/slowest_sync_clk]
  connect_bd_net -net clk_wiz_0_clk_out4 [get_bd_pins clk_wiz_0/clk_out4] [get_bd_pins lvds_buff_0/I]
  connect_bd_net -net clk_wiz_0_clk_out5 [get_bd_pins clk_wiz_0/clk_out5] [get_bd_pins fast_data_0/clkb]
  connect_bd_net -net clk_wiz_0_locked [get_bd_ports locked] [get_bd_pins clk_wiz_0/locked] [get_bd_pins proc_sys_reset_0/dcm_locked]
  connect_bd_net -net control_registers_v1_0_0_rd_register [get_bd_pins control_registers_v1_0_0/rd_register] [get_bd_pins fast_data_0/rd_register]
  connect_bd_net -net control_registers_v1_0_0_trigger [get_bd_pins control_registers_v1_0_0/trigger] [get_bd_pins slow_data_0/start]
  connect_bd_net -net fast_data_0_empty [get_bd_pins control_registers_v1_0_0/empty] [get_bd_pins fast_data_0/empty]
  connect_bd_net -net fast_data_0_error_register [get_bd_pins control_registers_v1_0_0/error_din] [get_bd_pins fast_data_0/error_register]
  connect_bd_net -net fast_data_0_fast_clk_n [get_bd_ports fast_clk_n] [get_bd_pins fast_data_0/fast_clk_n]
  connect_bd_net -net fast_data_0_fast_clk_p [get_bd_ports fast_clk_p] [get_bd_pins fast_data_0/fast_clk_p]
  connect_bd_net -net fast_data_0_full [get_bd_ports full] [get_bd_pins fast_data_0/full]
  connect_bd_net -net fast_data_0_match_out [get_bd_ports match_out] [get_bd_ports match_out_1] [get_bd_pins fast_data_0/match_out]
  connect_bd_net -net fast_data_0_stop_out [get_bd_ports stop_out] [get_bd_pins fast_data_0/stop_out]
  connect_bd_net -net lvds_buff_0_O [get_bd_ports slow_clk_p] [get_bd_pins lvds_buff_0/O]
  connect_bd_net -net lvds_buff_0_OB [get_bd_ports slow_clk_n] [get_bd_pins lvds_buff_0/OB]
  connect_bd_net -net proc_sys_reset_0_peripheral_reset [get_bd_pins fast_data_0/reset_p] [get_bd_pins proc_sys_reset_0/peripheral_reset]
  connect_bd_net -net proc_sys_reset_1_peripheral_aresetn [get_bd_pins axi_bram_ctrl_0/s_axi_aresetn] [get_bd_pins proc_sys_reset_1/peripheral_aresetn] [get_bd_pins processing_system7_0_axi_periph/M01_ARESETN]
  connect_bd_net -net proc_sys_reset_1_peripheral_reset [get_bd_pins clk_wiz_0/reset] [get_bd_pins proc_sys_reset_1/peripheral_reset] [get_bd_pins slow_data_0/reset_p]
  connect_bd_net -net processing_system7_0_FCLK_CLK0 [get_bd_pins axi_bram_ctrl_0/s_axi_aclk] [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins control_registers_v1_0_0/s00_axi_aclk] [get_bd_pins proc_sys_reset_1/slowest_sync_clk] [get_bd_pins processing_system7_0/FCLK_CLK0] [get_bd_pins processing_system7_0/M_AXI_GP0_ACLK] [get_bd_pins processing_system7_0_axi_periph/ACLK] [get_bd_pins processing_system7_0_axi_periph/M00_ACLK] [get_bd_pins processing_system7_0_axi_periph/M01_ACLK] [get_bd_pins processing_system7_0_axi_periph/S00_ACLK] [get_bd_pins rst_processing_system7_0_80M/slowest_sync_clk] [get_bd_pins slow_data_0/clk]
  connect_bd_net -net processing_system7_0_FCLK_RESET0_N [get_bd_pins proc_sys_reset_0/ext_reset_in] [get_bd_pins proc_sys_reset_1/ext_reset_in] [get_bd_pins processing_system7_0/FCLK_RESET0_N] [get_bd_pins rst_processing_system7_0_80M/ext_reset_in]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins proc_sys_reset_0/aux_reset_in] [get_bd_pins proc_sys_reset_1/aux_reset_in] [get_bd_pins rst_processing_system7_0_80M/aux_reset_in]
  connect_bd_net -net rst_processing_system7_0_80M_interconnect_aresetn [get_bd_pins processing_system7_0_axi_periph/ARESETN] [get_bd_pins rst_processing_system7_0_80M/interconnect_aresetn]
  connect_bd_net -net rst_processing_system7_0_80M_peripheral_aresetn [get_bd_pins control_registers_v1_0_0/s00_axi_aresetn] [get_bd_pins processing_system7_0_axi_periph/M00_ARESETN] [get_bd_pins processing_system7_0_axi_periph/S00_ARESETN] [get_bd_pins rst_processing_system7_0_80M/peripheral_aresetn]
  connect_bd_net -net slow_data_0_DOB [get_bd_pins axi_bram_ctrl_0/bram_rddata_a] [get_bd_pins slow_data_0/DOB]
  connect_bd_net -net slow_data_0_slow_data_n [get_bd_ports slow_data_n] [get_bd_pins slow_data_0/slow_data_n]
  connect_bd_net -net slow_data_0_slow_data_p [get_bd_ports slow_data_p] [get_bd_pins slow_data_0/slow_data_p]
  connect_bd_net -net trigger_1 [get_bd_ports trigger] [get_bd_pins fast_data_0/trigger_in]
  connect_bd_net -net trigger_button_1 [get_bd_ports trigger_button] [get_bd_pins fast_data_0/trigger_button]

  # Create address segments
  create_bd_addr_seg -range 0x10000 -offset 0x40000000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs axi_bram_ctrl_0/S_AXI/Mem0] SEG_axi_bram_ctrl_0_Mem0
  create_bd_addr_seg -range 0x10000 -offset 0x43C00000 [get_bd_addr_spaces processing_system7_0/Data] [get_bd_addr_segs control_registers_v1_0_0/s00_axi/reg0] SEG_control_registers_v1_0_0_reg0

  # Perform GUI Layout
  regenerate_bd_layout -layout_string {
   guistr: "# # String gsaved with Nlview 6.5.5  2015-06-26 bk=1.3371 VDI=38 GEI=35 GUI=JA:1.6
#  -string -flagsOSRD
preplace port DDR -pg 1 -y 40 -defaultsOSRD
preplace port slow_clk_n -pg 1 -y 800 -defaultsOSRD
preplace port full -pg 1 -y 600 -defaultsOSRD
preplace port slow_clk_p -pg 1 -y 780 -defaultsOSRD
preplace port match_out_1 -pg 1 -y 520 -defaultsOSRD
preplace port locked -pg 1 -y 710 -defaultsOSRD
preplace port trigger -pg 1 -y 690 -defaultsOSRD
preplace port fast_clk_n -pg 1 -y 560 -defaultsOSRD
preplace port slow_data_n -pg 1 -y 200 -defaultsOSRD
preplace port stop_out -pg 1 -y 640 -defaultsOSRD
preplace port D_n -pg 1 -y 730 -defaultsOSRD
preplace port match_out -pg 1 -y 500 -defaultsOSRD
preplace port fast_clk_p -pg 1 -y 540 -defaultsOSRD
preplace port FIXED_IO -pg 1 -y 730 -defaultsOSRD
preplace port slow_data_p -pg 1 -y 180 -defaultsOSRD
preplace port D_p -pg 1 -y 710 -defaultsOSRD
preplace port trigger_button -pg 1 -y 550 -defaultsOSRD
preplace port reset -pg 1 -y 350 -defaultsOSRD
preplace inst fast_data_0 -pg 1 -lvl 5 -y 580 -defaultsOSRD
preplace inst lvds_buff_0 -pg 1 -lvl 5 -y 790 -defaultsOSRD
preplace inst proc_sys_reset_0 -pg 1 -lvl 4 -y 580 -defaultsOSRD
preplace inst proc_sys_reset_1 -pg 1 -lvl 2 -y 420 -defaultsOSRD
preplace inst rst_processing_system7_0_80M -pg 1 -lvl 1 -y 350 -defaultsOSRD
preplace inst slow_data_0 -pg 1 -lvl 5 -y 180 -defaultsOSRD
preplace inst clk_wiz_0 -pg 1 -lvl 3 -y 630 -defaultsOSRD
preplace inst control_registers_v1_0_0 -pg 1 -lvl 3 -y 380 -defaultsOSRD
preplace inst axi_bram_ctrl_0 -pg 1 -lvl 3 -y 170 -defaultsOSRD
preplace inst processing_system7_0_axi_periph -pg 1 -lvl 2 -y 180 -defaultsOSRD
preplace inst processing_system7_0 -pg 1 -lvl 1 -y 120 -defaultsOSRD
preplace netloc processing_system7_0_DDR 1 1 5 NJ 40 NJ 40 NJ 40 NJ 40 NJ
preplace netloc trigger_button_1 1 0 5 NJ 550 NJ 550 NJ 480 NJ 480 1600
preplace netloc trigger_1 1 0 5 NJ 530 NJ 530 NJ 530 NJ 470 1610
preplace netloc fast_data_0_full 1 5 1 NJ
preplace netloc axi_bram_ctrl_0_bram_clk_a 1 3 2 NJ 140 NJ
preplace netloc axi_bram_ctrl_0_bram_addr_a 1 3 2 NJ 120 NJ
preplace netloc clk_wiz_0_locked 1 3 3 1260 720 NJ 720 NJ
preplace netloc processing_system7_0_axi_periph_M00_AXI 1 2 1 820
preplace netloc fast_data_0_fast_clk_n 1 5 1 NJ
preplace netloc processing_system7_0_M_AXI_GP0 1 1 1 N
preplace netloc slow_data_0_slow_data_n 1 5 1 NJ
preplace netloc fast_data_0_empty 1 2 4 880 290 NJ 290 NJ 310 1950
preplace netloc rst_processing_system7_0_80M_interconnect_aresetn 1 1 1 430
preplace netloc processing_system7_0_FCLK_RESET0_N 1 0 4 20 440 420 510 NJ 510 NJ
preplace netloc proc_sys_reset_1_peripheral_aresetn 1 1 2 460 320 830
preplace netloc fast_data_0_match_out 1 5 1 1970
preplace netloc fast_data_0_fast_clk_p 1 5 1 NJ
preplace netloc slow_data_0_slow_data_p 1 5 1 NJ
preplace netloc rst_processing_system7_0_80M_peripheral_aresetn 1 1 2 450 330 NJ
preplace netloc fast_data_0_error_register 1 2 4 880 470 NJ 430 NJ 430 1940
preplace netloc control_registers_v1_0_0_trigger 1 3 2 1180 150 NJ
preplace netloc D_p_1 1 0 5 NJ 560 NJ 560 NJ 490 NJ 450 NJ
preplace netloc proc_sys_reset_1_peripheral_reset 1 2 3 850 30 NJ 30 NJ
preplace netloc lvds_buff_0_OB 1 5 1 NJ
preplace netloc processing_system7_0_FIXED_IO 1 1 5 NJ 10 NJ 10 NJ 10 NJ 10 NJ
preplace netloc clk_wiz_0_clk_out1 1 3 2 1210 490 NJ
preplace netloc axi_bram_ctrl_0_bram_wrdata_a 1 3 2 N 160 NJ
preplace netloc axi_bram_ctrl_0_bram_rst_a 1 3 2 NJ 220 NJ
preplace netloc proc_sys_reset_0_peripheral_reset 1 4 1 1660
preplace netloc clk_wiz_0_clk_out2 1 3 2 1200 440 NJ
preplace netloc axi_bram_ctrl_0_bram_we_a 1 3 2 NJ 210 NJ
preplace netloc D_n_1 1 0 5 NJ 730 NJ 730 NJ 730 NJ 730 NJ
preplace netloc processing_system7_0_FCLK_CLK0 1 0 5 20 250 440 30 840 50 NJ 50 NJ
preplace netloc lvds_buff_0_O 1 5 1 NJ
preplace netloc clk_wiz_0_clk_out3 1 3 2 1250 460 NJ
preplace netloc reset_1 1 0 4 10 260 410 520 NJ 500 NJ
preplace netloc fast_data_0_stop_out 1 5 1 NJ
preplace netloc clk_wiz_0_clk_out4 1 3 2 NJ 680 1630
preplace netloc axi_bram_ctrl_0_bram_en_a 1 3 2 NJ 200 NJ
preplace netloc processing_system7_0_axi_periph_M01_AXI 1 2 1 810
preplace netloc slow_data_0_DOB 1 3 3 NJ 180 NJ 50 1940
preplace netloc control_registers_v1_0_0_rd_register 1 3 2 NJ 390 1640
preplace netloc clk_wiz_0_clk_out5 1 3 2 1220 670 NJ
levelinfo -pg 1 -10 220 650 1040 1440 1810 1990 -top 0 -bot 850
",
}

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


