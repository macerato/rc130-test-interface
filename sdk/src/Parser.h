/*
 * Parser.h
 *
 *  Created on: Jun 13, 2016
 *      Author: HEP
 */

#ifndef PARSER_H_
#define PARSER_H_

int parse(char*);
void rreg(u32);
char* help(void);
void wreg(u32, u32);
void ldmem();
void rmem(u32, u32, u32);
void writereadmem(u32, u32);
void read_fifo(void);
u32 empty(void);
void monitor(void);

#endif /* PARSER_H_ */
