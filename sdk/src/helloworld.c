/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_io.h"
#include "xil_types.h"
#include "Parser.h"
#include "bram_writer.h"
#include "sleep.h"

void write_command(u32, u32);
void disable_all_drivers();
void clock_config();
void enable_driver(u32);


u32 writePointer = XPAR_BRAM_0_BASEADDR;

// bram addr XPAR_AXI_BRAM_CTRL_0_S_AXI_BAwSEADDR

int main()
{
    init_platform();
    /*
     * 	        mem0  = 8'b00000000; //idle
	        mem1  = 8'b11001010; //start transmit cmd CA
	        mem2  = 8'b00000000; //space ... 0
	        mem3  = 8'b01101000; //end of reg 4 fast config 68
	        mem4  = 8'b00000000; 0
	        mem5  = 8'b00000000; 0
	        mem6  = 8'b00000000; 0
            mem7  = 8'b11001000; C8
            mem8  = 8'b00000000; //space... 0
            mem9  = 8'b00000000; //end of reg 2 start pattern 0
            mem10 = 8'b00000100; 4
            mem11 = 8'b00000010; 2
            mem12 = 8'b00000000; 0
            mem13 = 8'b11000100; C4
            mem14 = 8'b00000000; //space... 0
            mem15 = 8'b00011110; //end of lower 32 1E
            mem16 = 8'b00011110; 1E
            mem17 = 8'b00011110; 1E
            mem18 = 8'b01010100; 54
            mem19 = 8'b11000001; C1
            mem20 = 8'b00000000; //space... 0
            mem21 = 8'b10011100; //end or upper 32 9C
            mem22 = 8'b00010001; 11
            mem23 = 8'b01100010; 62
            mem24 = 8'b11101110; EE
            mem25 = 8'b11000001; C1
            mem26 = 8'b00000000; //space... 0
            mem27 = 8'b00000000; //end of reg 3 write start address 0
            mem28 = 8'b00000000; 0
            mem29 = 8'b00000000; 0
            mem30 = 8'b00000000; 0
            mem31 = 8'b11000110; C6
     */

    /*u8 memory[32] = {0xC6, 0x0, 0x0, 0x0, 0x0, 0x0, 0xC1, 0xEE, 0x62, 0x11, 0x9C, 0x0,
    		0xC1, 0x54, 0x1E, 0x1E, 0x1E, 0x0, 0xC4, 0x0, 0x4, 0x2, 0x0, 0x0, 0xC8,
    		0x0, 0x0, 0x0, 0x68, 0x0, 0xCA, 0x0};*/

    /*u8 memory[45] = {0xC6, 0x0, 0x0, 0x0, 0x0, 0x0, 0xC1, 0xEE, 0x62, 0x11, 0x9C, 0x0,
    		0xC1, 0x54, 0x1E, 0x1E, 0x1E, 0x0, 0xC0, 0xAA, 0xAA, 0xAA, 0xA0, 0x0,
    		0x0, 0xC0, 0xBB, 0xBB, 0xBB, 0xB0,
    		0x0, 0xC4, 0x0, 0x2, 0x2, 0x0, 0x0, 0xC8,
    		0x0, 0x0, 0x0, 0x68, 0x0, 0xCA, 0x0};

    int i = 0;
    while(i < 45)
    {
    	Xil_Out32(XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR + 32*i, memory[i]);
    	i++;
    }

	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x0000000F);
	usleep(1000);
	Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000000);*/

    //clock_config();
    Xil_Out32(XPAR_CONTROL_REGISTERS_V1_0_0_BASEADDR, 0x00000000);

    xil_printf("RC130 Interface Terminal Starting...\n\rType 'help' for command list\n\r");

    char* input = "";
    int res = 0;
    do
    {
    	gets(input);
    	res = parse(input);
    }while(res == 0);


    cleanup_platform();

    return 0;
}

//void clock_config()
//{
//	xil_printf("changing freq...\n\r");
//	Xil_Out32(XPAR_MMCM_DRP_FSM_0_0, 0x00000041);
//	Xil_Out32(XPAR_MMCM_DRP_FSM_0_0, 0x000F0041);
//}
