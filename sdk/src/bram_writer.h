/*
 * bream_writer.h
 *
 *  Created on: Jun 13, 2016
 *      Author: HEP
 */

#ifndef BRAM_WRITER_H_
#define BRAM_WRITER_H_

void enable_serialout(void);
void short_command(u32);
void command_40(u32, u32);
void reset(u32);
void cfg_driver(u32, u32, u32, u32);
void disable_all(void);
void cfg_drv_jmp(u32);
void writeMemory(u32[], u32, u8);
void enableMemoryRead(u8, u32, u32);
void disableMemoryRead(void);
void setWritePointer(u8);
void fclk_enable(int);
void enableTransmit(void);


#endif /* BRAM_WRITER_H_ */
