`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/11/2016 10:56:02 AM
// Design Name: 
// Module Name: reset_control
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reset_control(
        input resetn_internal,
        input reset_rtl,
        input axi_clk,
        input clk1,
        input clk2,
        input clk3,
        input clk4,
        input clk5,
        input clk6,
        input clk7,
        input oserdes_clk,
        output oserdes_reset,
        output interconnect_resetn,
        output axi_peripheral_resetn,
        output clk1_reset,
        output clk2_reset, 
        output clk3_reset,
        output clk4_reset,
        output clk5_reset,
        output clk6_reset,
        output clk7_reset,
        output mmcm_reset,
        input mmcm_locked
    );
    
    wire master_reset;
    assign master_reset = reset_rtl | !resetn_internal;
    
    reg oserdes_reset_reg;
    assign oserdes_reset = oserdes_reset_reg;
    
    wire all_resets_asserted;
    assign all_resets_asserted = clk1_reset & clk2_reset & clk3_reset & clk4_reset & clk5_reset & clk6_reset & clk7_reset & oserdes_reset & ~interconnect_resetn & ~axi_peripheral_resetn;
    
    wire reset_done;
    assign reset_done = ~clk1_reset & ~clk2_reset & ~clk3_reset & ~clk4_reset & ~clk5_reset & ~clk6_reset & ~clk7_reset & ~oserdes_reset & interconnect_resetn & axi_peripheral_resetn;;
    
    reg mmcm_reset_reg = 0;
    reg interconnect_resetn_reg = 1;
    reg axi_peripheral_resetn_reg = 1;
    
    reg clk1_reset_reg = 0;
    reg clk2_reset_reg = 0;
    reg clk3_reset_reg = 0;
    reg clk4_reset_reg = 0;
    reg clk5_reset_reg = 0;
    reg clk6_reset_reg = 0;
    reg clk7_reset_reg = 0;
    
    assign clk1_reset = clk1_reset_reg;
    assign clk2_reset = clk2_reset_reg;
    assign clk3_reset = clk3_reset_reg;
    assign clk4_reset = clk4_reset_reg;
    assign clk5_reset = clk5_reset_reg;
    assign clk6_reset = clk6_reset_reg;
    assign clk7_reset = clk7_reset_reg;
    
    assign mmcm_reset = mmcm_reset_reg;
    assign interconnect_resetn = interconnect_resetn_reg;
    assign axi_peripheral_resetn = axi_peripheral_resetn_reg;

    reg [5:0] counter = 0;
    
    localparam WAIT = 0,
               CLOCK_RESET = 1,
               WAIT_FOR_LOCK = 2,
               FULL_RESET = 3,
               RELEASE_INTERCONNECT = 4,
               RELEASE_RESETS = 5,
               DEASSERT = 6;

    wire [2:0] state;
    reg [2:0] state_reg = WAIT;
    assign state = state_reg;
    
    reg [2:0] next_state = WAIT;
    
    always @(posedge axi_clk) begin
        state_reg <= next_state;
    end 
    
    always @(*) begin
        case(state)
            WAIT: begin
                next_state = (master_reset == 1) ? CLOCK_RESET : WAIT;
            end
            CLOCK_RESET: begin
                next_state = (mmcm_locked == 0) ? WAIT_FOR_LOCK : CLOCK_RESET;
            end
            WAIT_FOR_LOCK: begin
                next_state = (mmcm_locked == 1) ? FULL_RESET : WAIT_FOR_LOCK;
            end
            FULL_RESET: begin
                next_state = (all_resets_asserted == 1) ? RELEASE_INTERCONNECT : FULL_RESET;          
            end
            RELEASE_INTERCONNECT: begin
                next_state = (counter == 15) ? RELEASE_RESETS : RELEASE_INTERCONNECT;
            end
            RELEASE_RESETS: begin
                next_state = (reset_done == 1) ? DEASSERT : RELEASE_RESETS;
            end
            DEASSERT: begin
                next_state = (master_reset == 0) ? WAIT : DEASSERT;
            end
            default: begin
                next_state = WAIT;
            end 
        endcase
    end


    // Counter logic
    always @(posedge axi_clk) begin
        if(state == RELEASE_INTERCONNECT) begin
            counter <= counter + 1;
        end else begin
            counter <= 5'b0;
        end
    end
    
    // ****** Asserting resets ******
    
    // mmcm resets
    always @(posedge axi_clk) begin
        if(state == CLOCK_RESET) begin
            mmcm_reset_reg <= 1;
        end else if(state == WAIT_FOR_LOCK) begin
            mmcm_reset_reg <= 0;
        end
    end
    
    // interconnect
    always @(posedge axi_clk) begin
            if(state == FULL_RESET) begin
                interconnect_resetn_reg <= 0;
            end else if(state == RELEASE_INTERCONNECT) begin
                interconnect_resetn_reg <= 1;
            end
    end

    // AXI peripherals
    always @(posedge axi_clk) begin
           if(state == FULL_RESET) begin
               axi_peripheral_resetn_reg <= 0;
           end else if(state == RELEASE_RESETS) begin
               axi_peripheral_resetn_reg <= 1;
           end
      end
      
     // All clock domains
     always @(posedge clk1) begin
        if(state == FULL_RESET) begin
             clk1_reset_reg <= 1;
         end else if(state == RELEASE_RESETS) begin
             clk1_reset_reg <= 0;
         end
      end
      
     // All clock domains
      always @(posedge clk2) begin
         if(state == FULL_RESET) begin
              clk2_reset_reg <= 1;
          end else if(state == RELEASE_RESETS) begin
              clk2_reset_reg <= 0;
          end
       end
       
          // All clock domains
       always @(posedge clk3) begin
          if(state == FULL_RESET) begin
               clk3_reset_reg <= 1;
           end else if(state == RELEASE_RESETS) begin
               clk3_reset_reg <= 0;
           end
        end
        
       // All clock domains
        always @(posedge clk4) begin
           if(state == FULL_RESET) begin
                clk4_reset_reg <= 1;
            end else if(state == RELEASE_RESETS) begin
                clk4_reset_reg <= 0;
            end
         end
         
         
    // All clock domains
         always @(posedge clk5) begin
            if(state == FULL_RESET) begin
                 clk5_reset_reg <= 1;
             end else if(state == RELEASE_RESETS) begin
                 clk5_reset_reg <= 0;
             end
          end
          
    // All clock domains
          always @(posedge clk6) begin
             if(state == FULL_RESET) begin
                  clk6_reset_reg <= 1;
              end else if(state == RELEASE_RESETS) begin
                  clk6_reset_reg <= 0;
              end
           end
           
           always @(posedge clk7) begin
                if(state == FULL_RESET) begin
                     clk7_reset_reg <= 1;
                 end else if(state == RELEASE_RESETS) begin
                     clk7_reset_reg <= 0;
                 end
              end
           
       always @(posedge oserdes_clk) begin
            if(state == FULL_RESET) begin
                 oserdes_reset_reg <= 1;
             end else if(state == RELEASE_RESETS) begin
                 oserdes_reset_reg <= 0;
             end
        end
endmodule
