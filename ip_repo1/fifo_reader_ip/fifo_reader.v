`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/15/2016 11:37:32 AM
// Design Name: 
// Module Name: fifo_reader
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module fifo_reader(
        input rd_register,
        output rd_en,
        input clk,
        input rst
    );
    
    localparam INIT = 0,
               READ = 1,
               WAIT = 2;
               
    wire [1:0] state;
    reg [1:0] state_reg = INIT;
    assign state = state_reg;
    
    reg [1:0] next_state = INIT;
    
    always @(posedge clk) begin
        if(!rst) state_reg <= next_state;
        else     state_reg <= INIT;
    end
    
    always @(*) begin
        case(state) 
            INIT: next_state = (rd_register == 1) ? READ : INIT;
            READ: next_state = WAIT;
            WAIT: next_state = (rd_register == 0) ? INIT : WAIT;
            default: next_state = INIT;
        endcase
    end
    
    assign rd_en = (state == READ) ? 1 : 0;
    
endmodule
