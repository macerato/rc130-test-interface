`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/08/2016 03:24:35 PM
// Design Name: 
// Module Name: lvds_buff
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lvds_buff(
        input I,
        output O,
        output OB
    );
    
    OBUFDS inst0(.I(I), .O(O), .OB(OB));
endmodule
