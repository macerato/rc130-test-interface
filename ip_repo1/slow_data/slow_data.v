`timescale 1ns / 1ps

module slow_data(
    input clk, 
    input reset_p,
    input start,
    input [15:0] ADDRB,
    input [31:0] DIB,
    input [3:0] WEB,
    input ENB,
    input CLKB,
    input RSTB,
    output [31:0] DOB,
    output slow_data_p,
    output slow_data_n
);

    // parallel byte that enters the serializer
    wire [31:0] slow_data_parallel;
    reg [7:0] slow_data_parallel_reg = 0;
    
    // Every 8 clock cycles, move the byte to be sent into a register
    always @(posedge clk) begin
        if(state == OUT7 && count_en == 6) begin
            slow_data_parallel_reg <= slow_data_parallel[7:0];
        end
    end

    // Signal that enables the bram readout, the bram contains the data to be 
    // sent
    wire bram_en;
    
    // The current address of the bram, the state machine will cycle through the
    // entire ram
    wire [9:0] bram_address;

    // single ended data from serializer
    wire slow_data;
    reg slow_data_reg = 0;
    
    // Slow data reg is moved into slow_dataa_reg_dly every clock cycle, this 
    // helps in meeting timing
    reg slow_data_reg_dly = 0;
    assign slow_data = slow_data_reg_dly;
    
    always @(posedge clk) begin
        slow_data_reg_dly <= slow_data_reg;
    end
    
    // Counter for clock enable. The in clk in 80 mhz but the slow clk is 
    // only 10 mhz so we use a clock enable for the state machine
    reg [2:0] count_en =3'b0;

    localparam WAITING = 0,
               OUT0 = 1,
               OUT1 = 2,
               OUT2 = 3,
               OUT3 = 4,
               OUT4 = 5,
               OUT5 = 6,
               OUT6 = 7,
               OUT7 = 8,
               RESET = 9,
               INIT = 10,
               LOAD = 11;

    wire [3:0] state;
    reg [3:0] state_reg = INIT;
    assign state = state_reg;

    reg [3:0] next_state = INIT;
    
    reg [9:0] bram_address_reg = 10'b0;
    assign bram_address = bram_address_reg;
    
    // The state machine advances every 8 clock cycles
    always@(posedge clk) begin
        if(reset_p) begin
            state_reg <= RESET;
        end else begin
            count_en <= count_en + 1;
            if(count_en == 3'd7) state_reg <= next_state;
        end
    end 

    // once the start signal is asserted, cycle through the slow_data_parallel
    // register and output a bit on the serial line.
    always @(*) begin
        case(state)
            INIT: begin
                next_state = (start == 0) ? WAITING : INIT;
                slow_data_reg = 0;
            end
            WAITING: begin
                next_state = (start == 1'b1) ? LOAD : WAITING;
                slow_data_reg = 0;
            end
            LOAD: begin
                next_state = OUT0;
                slow_data_reg = 0;
            end
            OUT0: begin
                next_state = OUT1;
                slow_data_reg = slow_data_parallel_reg[7];
            end
            OUT1: begin
                next_state = OUT2;
                slow_data_reg = slow_data_parallel_reg[6];
            end
            OUT2: begin
                next_state = OUT3;
                slow_data_reg = slow_data_parallel_reg[5];
            end
            OUT3: begin
                next_state = OUT4;
                slow_data_reg = slow_data_parallel_reg[4];
            end
            OUT4: begin
                next_state = OUT5;
                slow_data_reg = slow_data_parallel_reg[3];
            end
            OUT5: begin
                next_state = OUT6;
                slow_data_reg = slow_data_parallel_reg[2];
            end
            OUT6: begin
                next_state = OUT7;
                slow_data_reg = slow_data_parallel_reg[1];
            end
            OUT7: begin
                next_state = (bram_address_reg == 10'd0) ? INIT : OUT0;
                slow_data_reg = slow_data_parallel_reg[0];
            end
            RESET: begin
                next_state = INIT;
                slow_data_reg = 0;
            end
            default: begin
                next_state = RESET;
                slow_data_reg = 0;
            end
        endcase
    end

    // increment the bram every time we need a new byte
    always @(posedge clk) begin
        if(reset_p) begin
            bram_address_reg <= 0;
        end else if(state == OUT1 && count_en == 7) begin
            bram_address_reg <= bram_address_reg + 4;
        end
    end

    assign bram_en = (state == OUT1 || state == LOAD);
    
    // bram that holds the data we want to send. It is loaded by the processor
    // over the AXI bus
    BRAM_TDP_MACRO #(
        .BRAM_SIZE("36Kb"),
        .DEVICE("7SERIES"),
        .DOA_REG(0),
        .DOB_REG(0),
        .INIT_A(0),
        .INIT_B(0),
        .INIT_FILE("NONE"),
        .READ_WIDTH_A(32),
        .READ_WIDTH_B(32),
        .WRITE_WIDTH_A(32),
        .WRITE_WIDTH_B(32)
    ) BRAM_inst (
        .DOA(slow_data_parallel),
        .DOB(DOB),
        .ADDRA(bram_address),
        .ADDRB(ADDRB[9:0]),
        .CLKA(clk),
        .CLKB(CLKB),
        .DIA(32'b0),
        .DIB(DIB),
        .ENA(bram_en),
        .ENB(ENB),
        .REGCEA(1'b0),
        .REGCEB(1'b0),
        .RSTA(reset_p),
        .RSTB(RSTB),
        .WEA(4'b0000),
        .WEB(WEB)
    );
    
    // Slow data LVDS output buffer
    OBUFDS slow_data_lvds_out(.I(slow_data), .O(slow_data_p), .OB(slow_data_n));
    

endmodule