`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/19/2016 10:32:36 AM
// Design Name: 
// Module Name: error_counter
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module error_counter(
        input CLK,
        input reset,
        input set,
        output [37:0] Q
    );
    
    reg [46:0] counter = 0;
    assign Q = counter[46:9];
    
    always @(posedge CLK) begin
        if(set && !reset) counter <= counter + 1;
        else if(reset) counter <= 47'b0;
    end
    
endmodule
