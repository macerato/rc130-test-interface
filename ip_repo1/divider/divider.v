`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/07/2016 02:43:14 PM
// Design Name: 
// Module Name: divider
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module divider(
        input clk_in,
        output clk_out
    );
    
    reg [2:0] counter = 3'b0;
    
    always @(posedge clk_in) begin
        counter <= counter + 1'b1;
    end
    
    assign clk_out = counter[2];
    
endmodule
