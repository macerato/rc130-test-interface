`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 06/02/2016 05:50:10 PM
// Design Name: 
// Module Name: top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bram_reader(
        output wire [31:0] addr,
        input wire [31:0] din,
        output wire en,
        input wire rst_n,
        input wire clk,
        output wire [7:0] dout,
        input wire trigger,
        input wire en_in
    );
    
    wire en_out;
    reg en_dly = 1'b0;
    reg en_reg = 1'b0;
    
    assign en_out = en_reg & ~en_dly;
    
    always @(posedge clk) begin
        en_reg <= en_in;
        en_dly <= en_reg;
    end
    
    wire lock;
    reg lock_reg = 1'b1;
    assign lock = lock_reg;
    
    always @(posedge clk) begin
        if(en_out) begin
            lock_reg <= 0;
        end else if(addr == 256*32 ) begin
            lock_reg <= 1;
        end else if(CurrentState == RESET) begin
            lock_reg <= 1;
        end
    end
    
    reg [31:0] addr_ptr = 32'b0;
        
    // one shot for trigger
    wire trigger_out;
    reg trigger_dly = 1'b0;
    reg trigger_reg = 1'b0;
    
    assign trigger_out = trigger_reg & ~trigger_dly;
    
    always @(posedge clk) begin
        trigger_reg <= trigger;
        trigger_dly <= trigger_reg;
    end
    
    /// FSM begin
    reg [7:0] dout_reg = 32'b0;
    
    localparam INITIAL = 2'b00,
               SET_BRAM_SIGNALS = 2'b01,
               READ_BRAM_OUTPUT =  2'b10,
               RESET = 2'b11;
               
    reg [1:0] CurrentState = INITIAL;
    reg [1:0] NextState = INITIAL;
    
    always @(posedge clk) begin
        if(!rst_n) CurrentState <= RESET;
        else       CurrentState <= NextState;
    end 
    
    always @* begin
        case(CurrentState)
            INITIAL: begin
                NextState = (trigger_out && !lock) ? SET_BRAM_SIGNALS : INITIAL;
            end
            SET_BRAM_SIGNALS: begin
                NextState = READ_BRAM_OUTPUT;
            end
            READ_BRAM_OUTPUT: begin
                NextState = INITIAL;
            end
            RESET: begin
                NextState = INITIAL;
            end
            default: begin
                NextState = RESET;
            end
        endcase
    end
    
    always @(posedge clk) begin
        if(CurrentState == READ_BRAM_OUTPUT) begin
            dout_reg <= {din[0], din[1], din[2], din[3], din[4], din[5], din[6], din[7]};
         end else if(CurrentState == RESET) begin
            dout_reg <= 8'b0;
        end
    end
    
    always @(posedge clk) begin
        if(CurrentState == READ_BRAM_OUTPUT && addr_ptr < 256*32) begin
            addr_ptr <= addr_ptr + 6'd32;
        end else if(CurrentState == READ_BRAM_OUTPUT) begin
            addr_ptr <= 32'b0;
        end else if(CurrentState == RESET) begin
            addr_ptr <= 32'b0;
        end else if(lock == 1) begin
            addr_ptr <= 32'b0;
        end 
    end
    
    assign addr = addr_ptr;
    assign en = (CurrentState == SET_BRAM_SIGNALS) ? 1'b1 : 1'b0;
    assign dout = dout_reg;
    
endmodule
