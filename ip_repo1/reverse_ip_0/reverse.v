`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/14/2016 11:21:31 AM
// Design Name: 
// Module Name: reverse
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module reverse(
        input [7:0] din,
        output [7:0] dout
    );
    
    assign dout[7] = din[0];
    assign dout[6] = din[1]; 
    assign dout[5] = din[2];
    assign dout[4] = din[3];
    assign dout[3] = din[4];
    assign dout[2] = din[5];
    assign dout[1] = din[6];
    assign dout[0] = din[7];    
    
endmodule
