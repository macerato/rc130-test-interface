`timescale 1ns / 1ps
///////////////////////////////////////////////////////////////////////////
/// File: fast_data.v
/// Author: Mark Macerato (macerato@sas.upenn.edu)
/// Description: This module contains the mechanism for evaluating the high speed
/// memory readout from the repeater chip. It contains a deserializer that
/// processes the incoming data stream, which is then compared to an expected 
/// pattern loaded 
////////////////////////////////////////////////////////////////////////////

module fast_data(
        input clk,
        input clkdiv,
        input fast_clk,
        input reset_p,
        input trigger_in,
        input trigger_button,
        input D_p,
        input D_n,
        input clkb,
        input rd_register,
        output match_out,
        output fast_clk_p,
        output fast_clk_n,
        output [63:0] error_register,
        output full,
        output empty,
        output stop_out
    );
    
    // the readout process begins with a trigger either from a button or IO 
    wire trigger;
    assign trigger = trigger_in | trigger_button;
    
    // The signal indicating that the fast_data module has acquired a lock on the
    // incoming data
    wire stop_out;
    assign stop_out = stop;
    
    // header file containing the data to compare with. As of now, the only way to 
    // change the pattern is to change the register contained in this file
    `include "memory.vh";
        
    // The incoming differential fast data signal is converted into a single ended
    // signal
    wire D;
    IBUFDS lvds_buffer(.I(D_p), .IB(D_n), .O(D));
    
    // The parallel word that emerges from the serializer
    wire [7:0] parallel_din;
    
    // The parallel word parallel_din is loaded into parallel_din_dly after a 
    // clock cycle. This is done to avoid timing violations.
    wire [7:0] parallel_din_dly;
    reg [7:0] parallel_din_reg = 0;
    assign parallel_din_dly = ~parallel_din_reg;
    assign parallel_data_out = parallel_din_dly;
    
    // Every time a byte is loaded in by the SERDES, move it over to parallel_din_reg
    // As mentioned above, this helps meet timing
    always @(posedge clkdiv) begin
        if(reset_p) begin
            parallel_din_reg <= 0;
        end else begin
            parallel_din_reg <= parallel_din;
        end
    end
    
    // The SERDES bitslip signal. When asserted for a clock cycle, the incoming data
    // is shifted by one bit
    wire bitslip;
    
    // Once the trigger comes in, the trigger_delay module waits some time 
    // and then asserts trigger_set indefinitely.
    wire trigger_set;
    
    // When the trigger arives, wait until the data stream begins, then assert
    // trigger_set. In continuous readout this is not important
    trigger_delay trig_inst(
        .en(trigger_set),
        .clk(clkdiv),
        .reset_p(reset_p),
        .trigger(trigger)
    );
    
    // Input SERDES
    ISERDESE2 #(
        .DATA_RATE("DDR"),
        .DATA_WIDTH(8),
        .INTERFACE_TYPE("NETWORKING"),
        .NUM_CE(1)
    ) ser_inst (
        .Q1(parallel_din[0]),
        .Q2(parallel_din[1]),
        .Q3(parallel_din[2]),
        .Q4(parallel_din[3]),
        .Q5(parallel_din[4]),
        .Q6(parallel_din[5]),
        .Q7(parallel_din[6]),
        .Q8(parallel_din[7]),
        .BITSLIP(bitslip),
        .CLK(clk),
        .CLKB(~clk),
        .CLKDIV(clkdiv),
        .D(D),
        .RST(reset_p),
        .CE1(1'b1)
    );
    
    
    // The signal which indicates that a lock was acquired and the test pattern
    // was located
    wire stop;
    
    // Once trigger_set is asserted, this emits a pulse on bitslip every full read
    // of the memory until stop is asserted. In other words, keep bitslipping the
    // data until we find the test pattern
    bitslip_gen bitslip_inst(
        .clk(clkdiv),
        .set(trigger_set),
        .stop(stop),
        .bitslip(bitslip)
    );
    
    
    // This module waits to see the test pattern on parallel_din_dly. It needs to see
    // it four cycles in a row - for now the pattern is DE,AD,BE,EF. Once it is
    // found, the stop signal is asserted
    alignment align_inst(
        .clk(clkdiv),
        .reset(reset_p),
        .din(parallel_din_dly),
        .stop(stop)
    );
    
    // The address of the rom register stored in the header file. This is the 
    // dataset that we're comparing to
    wire [9:0] rom_address;
    reg  [9:0] rom_address_reg = 0;
    assign rom_address = rom_address_reg;
    
    // Every byte that comes in, we increment the address to compare to once we 
    // have the stop signal. So once the alignment block acquires the pattern,
    // we can start comparing
    always @(posedge clkdiv) begin
        if(reset_p) begin
            rom_address_reg <= 10'b0;
        end
        else if(stop) begin
            rom_address_reg <= rom_address_reg + 1;
        end
    end
   
    // The data at the current rom address is moved into a register for comparison
    // with the incoming data
    reg [7:0] rom_out_reg = 0;
    wire [7:0] rom_out;
    assign rom_out = rom_out_reg;
    always @(posedge clkdiv) begin
        rom_out_reg <= rom[1023 - rom_address];
    end
    
    // Whether or not the incoming data matches the stored pattern in the header file
    wire match;
    assign match = (rom_out == parallel_din_dly);
    assign match_out = match;
    
    // fast clock differential output
    OBUFDS fclkout(.I(fast_clk), .O(fast_clk_p), .OB(fast_clk_n));
    
    ///////////////////
    
    // Count the number of bytes since we acquired the lock. This will be used for
    // error reporting
    reg [47:0] timer = 0;
    always @(posedge clkdiv) begin
        if(reset_p) begin
            timer <= 48'b0;
        end else if(stop) begin
            timer <= timer + 1;
        end
    end
    
    // Error is a 64 bit word which records errors in the input stream
    // When match goes low, there is an error. The error register contains
    // the time of error, the data which just came in incorrectly, and the 
    // value it was being compared to
    wire [63:0] error;
   assign error = (timer << 16) | (parallel_din_dly << 8) | rom_out_reg;

    // Read enable for the fifo that holds the errors
    wire rd_en;
    
    // Fifo that holds the errors. Everytime the match bit goes low after the 
    // test pattern was acquired, write enable is asserted and the error 
    // register is loaded into the fifo
    FIFO_SYNC_MACRO #(
        .DATA_WIDTH(64),
        .DEVICE("7SERIES"),
        .FIFO_SIZE("36Kb")
    ) error_fifo (
        .DO(error_register),
        .EMPTY(empty),
        .FULL(full),
        .DI(error),
        .RDEN(rd_en),
        .RST(reset_p),
        .CLK(clkdiv),
        .WREN(!match &&  stop)
    );
    
    // Module that enables the processor to read out the fifo, Once the rd_register
    // signal is asserted, the module reads out one element of the fifo. 
    // It is read out to the error_register variable.
    // The rd_register and error_register variables are connected to the 
    // control registers available to the processor in the top level design
    fifo_reader fifo_reader_inst(
        .rd_register(rd_register),
        .clk(clkdiv),
        .rst(reset_p),
        .rd_en(rd_en)
    );
endmodule
