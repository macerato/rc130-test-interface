`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/07/2016 03:16:26 PM
// Design Name: 
// Module Name: receiver2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module bitslip_gen(
    input clk,
    input stop,
    input set,
    output bitslip
    );
    
    // Every 'length' clock cyles, assert bitslip for one clkdiv cycle. This will
    // cause the ISERDES to shift its data frame
    // bitslip needs to be high when clkdiv goes high at the SERDES, so we count
    // on negative edges here
    
    parameter length = 1025;
    
    reg [11:0] counter = 0;
    reg bitslip_reg = 0;
    assign bitslip = bitslip_reg;
    
    always @(negedge clk) begin
        if(set) counter <= counter + 1;
        if(!stop && counter == length) bitslip_reg <= 1;
        else                           bitslip_reg <= 0; 
    end
endmodule
