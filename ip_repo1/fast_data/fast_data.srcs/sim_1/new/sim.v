`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/01/2016 04:16:48 PM
// Design Name: 
// Module Name: sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sim();

    wire clk, clkb, clkdiv, reset_p, trigger, D_p, D_n, match;
    wire [7:0] parallel_data_out;
    reg clk_reg, clkdiv_reg, reset_p_reg, trigger_reg, D_p_reg, D_n_reg;
    
    assign clk = clk_reg;
    assign clkdiv = clkdiv_reg;
    assign reset_p = reset_p_reg;
    assign trigger = trigger_reg;
    assign D_p = D_p_reg;

    assign D_n = ~D_p;

    fast_data dut(
        .clk(clk),
        .clkdiv(clkdiv),
        .reset_p(reset_p),
        .trigger(trigger),
        .parallel_data_out(parallel_data_out),
        .D_p(D_p),
        .D_n(D_n),
        .match(match)
    );
    
    
    reg [5:0] count = 0;
    reg [63:0] data = 64'h1c2d5cBADEADBEEF;
    always  begin
        #2.125;
        D_p_reg <= data[63 - count];
        count <= count + 1;
        #1.000;
    end
    
    initial begin
        clk_reg <= 0;
        clkdiv_reg <= 0; 
        reset_p_reg <= 0;
        trigger_reg <= 0; 
        D_p_reg <= 0; 
        
        #3003.125 reset_p_reg <= 1;
        #100 reset_p_reg <= 0;
        #3023 trigger_reg <= 1;
        #100 trigger_reg <= 0;
        
        
    end
    
    assign clkb = ~clk;
    always begin
        #3.125 clk_reg <= ~clk_reg;
    end
    always begin
        #3.125 clkdiv_reg <= ~clkdiv_reg;
        #9.375;
    end
endmodule
